
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:app/src/app.template.dart' as ng;
import 'package:app/src/core/dime_process_service.dart';

import 'main.template.dart' as self;
import 'dart:js';

@GenerateInjector(routerProviders)
final InjectorFactory injector = self.injector$Injector;

@GenerateInjector(routerProvidersHash)
final InjectorFactory injectorLocal = self.injectorLocal$Injector;

void main() {
  final local = const bool.fromEnvironment('local', defaultValue: true);
context.callMethod('registerLogMiddleware', ['${DIMEProcessService.getBaseUrl()}']);
  print(local);
  if (local) {
    runApp(ng.AppComponentNgFactory, createInjector: injectorLocal);
  } else {
    runApp(ng.AppComponentNgFactory, createInjector: injector);
  }
}
