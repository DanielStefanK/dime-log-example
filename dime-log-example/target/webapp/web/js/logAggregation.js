//js


const getMetaData = () => {
    return {
        windowHeight: window.innerHeight,
        windowWidth: window.innerWidth,
        browserVersion: navigator.appVersion,
        fullUrl: window.location.href
    }
}


const registerLogMiddleware = (endpoint) => {
    console.log ("register middleware")
    const realLogger = window.console.error
    // queue for bulk sending
    let logs = []
    let timeout;

    window.console.error = (msg) => {
        realLogger(msg)
        // add this log to the messages
        logs.push({
            ...getMetaData(),
            log: msg,
            timestamp: Date.now(),
        })

        // if there is no send scheduled create one
        if (!timeout) {
            timeout = setTimeout(() => {
                fetch(
                    `${endpoint}/rest/_logging`,
                    {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json;charset=utf-8'
                        },
                        body: JSON.stringify({logs})
                    })

                logs = []
                timeout = undefined;
            }, 1000)
        }
    }

}

window.registerLogMiddleware = registerLogMiddleware