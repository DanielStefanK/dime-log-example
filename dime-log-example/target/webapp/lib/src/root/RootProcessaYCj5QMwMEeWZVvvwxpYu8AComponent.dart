// routing wrapper for the Home process
import 'package:angular/angular.dart';	
import 'package:angular_router/angular_router.dart';
import 'dart:html';
import 'dart:convert';
import 'package:app/src/core/AbstractRoutes.dart';
import 'package:app/src/core/dime_process_service.dart';
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/notification/notification_component.dart';

//Data
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/data/todo.dart' as todo;
//Process
import 'package:app/src/process/ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent.dart';
//Deserializer
import 'package:app/src/models/UserInteractionHome_YCj5QMwMEeWZVvvwxpYu8AResponseDeserializer.dart';

@Component(
  	selector: 'root-Home-aYCj5QMwMEeWZVvvwxpYu8A-process',
  	directives: const [coreDirectives,ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent,login.Login],
	template: '''
	<login-form 
		*ngIf="showLogin"
		(signedin)="retry()"
		[modal]="false"
	></login-form>
	<Home-aYCj5QMwMEeWZVvvwxpYu8A-process
		*ngIf="!showLogin&&runtime!=null"
		[runtimeId]="runtimeId"
		[parentRuntimeId]="parentRuntimeId"
		[majorSIB]="runtime.majorSIB"
		[minorSIB]="runtime.minorSIB"
		[majorInput]="runtime.majorInput"
		[minorInput]="runtime.minorInput"
		[deserializer]="deserializer"
	>
	</Home-aYCj5QMwMEeWZVvvwxpYu8A-process>
	'''
)
class RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponent extends RootProcess implements OnInit {
	
	
	@Input()
	String startPointId;
	
	@Input()
	String parentRuntimeId;
	
	final DIMEProcessService _processService;
	
	final Router _router;
	
	final UserInteractionHome_YCj5QMwMEeWZVvvwxpYu8AResponseDeserializer deserializer = new UserInteractionHome_YCj5QMwMEeWZVvvwxpYu8AResponseDeserializer();
	
	final NotificationService _notificationService;
	
	@override
	UserInteractionResponseDeserializer getDeserializer() => deserializer;
	
	RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponent(this._processService,this._router,this._notificationService, AbstractRoutes routes): super(routes);
	
	void retry() {
		retryAfterLogin(_processService,"Home_YCj5QMwMEeWZVvvwxpYu8A");
	}
	
	@override
	ngOnInit() {
		
		if(startPointId==null||startPointId=='aYDBMQMwMEeWZVvvwxpYu8A') {
			Map<Object,dynamic> cache = new Map();
			_processService.startProcess(
				'Home_YCj5QMwMEeWZVvvwxpYu8A/public',
				deserializer,
				{
				},
				parentRuntimeId:parentRuntimeId
			)
			.then((sr)=>processResponse(_processService,sr))
			.catchError((e)=>processError(e));
		
		}
		 else {
			print("Cannot start process Home with start point id ${startPointId}");
		}
	}
	
	@override
	Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
	
	@override
	String getParentRuntimeId() => parentRuntimeId;
	
	@override
	String getGUIId() => null;
	
	@override
	Router getRouter() => _router;
	
	@override
	NotificationService getNotificationService() => _notificationService;
}

@Component(
	  	selector: 'root-Home-aYCj5QMwMEeWZVvvwxpYu8A-process',
	  	directives: const [coreDirectives,ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent,login.Login],
		template: '''
		<login-form 
			*ngIf="showLogin"
			(signedin)="retry()"
			[modal]="false"
		></login-form>
		<Home-aYCj5QMwMEeWZVvvwxpYu8A-process
			*ngIf="!showLogin&&runtime!=null"
			[runtimeId]="runtimeId"
			[parentRuntimeId]="parentRuntimeId"
			[majorSIB]="runtime.majorSIB"
			[minorSIB]="runtime.minorSIB"
			[majorInput]="runtime.majorInput"
			[minorInput]="runtime.minorInput"
			[guiId]="guiId"
			[sibId]="sibId"
			[deserializer]="deserializer"
		>
		</Home-aYCj5QMwMEeWZVvvwxpYu8A-process>
		'''
	)
	class RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponentSIB extends RootProcess implements AfterChanges {
		
		
		@Input()
		String startPointId;
		
		@Input()
		String sibId;
		
		@Input()
		String guiId;
		
		@Input()
		String parentRuntimeId;
		
		
		final DIMEProcessService _processService;
		
		final NotificationService _notificationService;
		
		final Router _router;
		
		final UserInteractionHome_YCj5QMwMEeWZVvvwxpYu8AResponseDeserializer deserializer = new UserInteractionHome_YCj5QMwMEeWZVvvwxpYu8AResponseDeserializer();
		
		@override
		UserInteractionResponseDeserializer getDeserializer() => deserializer;
		
		RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponentSIB(this._processService,this._router,this._notificationService, AbstractRoutes routes): super(routes);
		
		void retry() {
			retryAfterLogin(_processService,"Home_YCj5QMwMEeWZVvvwxpYu8A",sibId:sibId);
		}
		
		@override
		ngAfterChanges() {
			restart();
		}
		
		
		void restart() {
			if(startPointId==null||startPointId=='aYDBMQMwMEeWZVvvwxpYu8A') {
				Map<Object,dynamic> cache = new Map();
				_processService.startProcess(
					"Home_YCj5QMwMEeWZVvvwxpYu8A/${sibId}/public",
					deserializer,
					{
					},
					parentRuntimeId:parentRuntimeId,
					guiId:guiId
				)
				.then((sr)=>processResponse(_processService,sr))
				.catchError((e)=>processError(e));
			
			} else {
				print("Cannot start process Home with start point id ${startPointId}");
			}
		}
		
		@override
		Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
		
		@override
		String getParentRuntimeId() => parentRuntimeId;
		
		@override
		String getGUIId() => guiId;
		
		@override
		Router getRouter() => _router;
		
		@override
		NotificationService getNotificationService() => _notificationService;
	}

