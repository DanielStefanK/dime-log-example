import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services

import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
//GUI plug in component imports
//GUI SIB imports
//Embedded process SIB imports

import 'package:app/src/core/Validators.dart' as validators;

// Form placed in GUI model AddList
@Component(
  selector: 'form-_ArJncPvvEeWWodZOLky4iQ-addList',
  viewProviders: const [FORM_BINDINGS],
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  ],
  templateUrl: 'Form_ArJncPvvEeWWodZOLky4iQAddList.html'
)
class Form_ArJncPvvEeWWodZOLky4iQAddList extends dime.DIMEComponent implements OnInit, AfterViewChecked {
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	
  	
  	
  	
  	
  	// branch Add List as info.scce.dime.gui.editor.graphiti.api.CButton@fdaa9e1f (id: _ArN44PvvEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@6553cc0f (id: _hGBmcN7nEeW7MO8ocVWj8Q)
  	@Output('actionaddlistevent') Stream<Map<String,dynamic>> get evt_actionaddlistevent => actionaddlistevent.stream;
  	StreamController<Map<String,dynamic>> actionaddlistevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	
  	//Form load variable
  		@Input()
  		todo.TODOList
  		 formLoad;
  	//Global Scope
  	//DATA CONTEXT
  	//TODOList newList
  		@Input()
  		todo.TODOList attr_newList;
  	//Extra input variables
	
	//FORM
	ControlGroup form_ArJncPvvEeWWodZOLky4iQForm;
	bool formLoaded = false;
	//Form Inputs:
	// input field: Description
	String input__BNRdEPvvEeWWodZOLky4iQ;

	
	@Output('complex_attr_newList_update') Stream<dynamic> get complex_attr_newList_update => _complex_attr_newList_update.stream;
	StreamController<dynamic> _complex_attr_newList_update = new StreamController();

  	Form_ArJncPvvEeWWodZOLky4iQAddList(DIMEProcessService this.processService,Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		
			  	
		// branch Add List declaration
		if(this.actionaddlistevent!=null) {
			  			this.actionaddlistevent = new StreamController<Map<String,dynamic>>();  				
		}
			
	}
	
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
		}
		updateImageHash();
	}
	
	void updateInputs(
	)
	{
		
		updateWithoutInputs(updateHidden:false);
		loadFormFieldValues();
		
	}
	
	void loadFormFieldValues() {
		
	}
	
	void ngAfterViewChecked() {
		if(html.querySelector("#form${this.form_ArJncPvvEeWWodZOLky4iQForm.hashCode}")!=null&&!formLoaded) {
			formLoaded = true;
		}
	}
	
	
	/// called after the input variables are loaded, but before the template rendering
	void ngOnInit()
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		
		
		//FORM Field grouping and vaalidation
		this.form_ArJncPvvEeWWodZOLky4iQForm = FormBuilder.controlGroup({
			"input__BNRdEPvvEeWWodZOLky4iQ": [
				""
				,
				Validators.compose([
					
				])
				]
		});
		
		//FORM Field loading
		this.loadFormFieldValues();
	}
  
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Add List is clicked
  			void actionaddlisteventaArN44PvvEeWWodZOLky4iQEventTrigger(dynamic formValues)
  		
  		{
  			//Submit Form Values
  			this.form_ArJncPvvEeWWodZOLky4iQFormSubmit(formValues);
  			Map<String,dynamic> data = new Map();
  			data['newList'] = this.attr_newList;
  			this.actionaddlistevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		todo.TODOList
  		 initOnDemandattr_newList()
  		{
  			if(this.attr_newList==null){
  				this.attr_newList = new todo.TODOList
  				();
  				this._complex_attr_newList_update.add(this.attr_newList);
  			}
  			return this.attr_newList;
  		}
  		void setValueattr_newList(todo.TODOList
  		 value)
  		{
  			this.attr_newList = value;
  			this._complex_attr_newList_update.add(this.attr_newList);
  		}
  		void attr_newListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValueattr_newList(value);
  		}
  		
  	
  		
  		
  		
	// Triggered on Form Submit
	void form_ArJncPvvEeWWodZOLky4iQFormSubmit(Map formValues) {
	// Store Form Data in Attributes
	// input field Description with type: Text
	if(this.input__BNRdEPvvEeWWodZOLky4iQ!=null){
		if(this.input__BNRdEPvvEeWWodZOLky4iQ.toString().isNotEmpty){
			this.initOnDemandattr_newList().setValuedescription(
			input__BNRdEPvvEeWWodZOLky4iQ
			);
		} else {
			this.initOnDemandattr_newList().setValuedescription('');
		}
	} else {
		this.initOnDemandattr_newList().setValuedescription('');
	}
	}
		
	
}
