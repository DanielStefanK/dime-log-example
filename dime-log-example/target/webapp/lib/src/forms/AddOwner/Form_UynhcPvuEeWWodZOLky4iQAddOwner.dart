import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services

import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
//GUI plug in component imports
//GUI SIB imports
//Embedded process SIB imports

import 'package:app/src/core/Validators.dart' as validators;

// Form placed in GUI model AddOwner
@Component(
  selector: 'form-_UynhcPvuEeWWodZOLky4iQ-addOwner',
  viewProviders: const [FORM_BINDINGS],
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  ],
  templateUrl: 'Form_UynhcPvuEeWWodZOLky4iQAddOwner.html'
)
class Form_UynhcPvuEeWWodZOLky4iQAddOwner extends dime.DIMEComponent implements OnInit, AfterViewChecked {
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	
  	
  	
  	
  	
  	// branch Add Owner as info.scce.dime.gui.editor.graphiti.api.CButton@7ef22a89 (id: _UytoEPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@a59fcabe (id: _LIjH8NvFEeWsF7MALrkAZQ)
  	@Output('actionaddownerevent') Stream<Map<String,dynamic>> get evt_actionaddownerevent => actionaddownerevent.stream;
  	StreamController<Map<String,dynamic>> actionaddownerevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	
  	//Global Scope
  	//DATA CONTEXT
  	//ConcreteUser users
  		@Input()
  		DIMEList<todo.ConcreteUser> users;
  	//ConcreteUser chosenUser
  		@Input()
  		todo.ConcreteUser chosenUser;
  	//TODOList currentList
  		@Input()
  		todo.TODOList currentList;
  	//Extra input variables
	
	//FORM
	ControlGroup form_UynhcPvuEeWWodZOLky4iQForm;
	bool formLoaded = false;
	//Form Inputs:
	todo.ConcreteUser
	 input__LGLJ4Py1EeWB0q9jTolRA;
	//Select Add Owner Choice values
	DIMEList<todo.ConcreteUser>
	 input__LGLJ4Py1EeWB0q9jTolRAIteratable;

	
	@Output('complex_chosenUser_update') Stream<dynamic> get complex_chosenUser_update => _complex_chosenUser_update.stream;
	StreamController<dynamic> _complex_chosenUser_update = new StreamController();
	@Output('complex_currentList_update') Stream<dynamic> get complex_currentList_update => _complex_currentList_update.stream;
	StreamController<dynamic> _complex_currentList_update = new StreamController();

  	Form_UynhcPvuEeWWodZOLky4iQAddOwner(DIMEProcessService this.processService,Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		
			  	
		// branch Add Owner declaration
		if(this.actionaddownerevent!=null) {
			  			this.actionaddownerevent = new StreamController<Map<String,dynamic>>();  				
		}
			
	}
	
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
		}
		updateImageHash();
	}
	
	void updateInputs(
	{DIMEList<todo.ConcreteUser>  pusers,
	todo.TODOList pcurrentList
	})
	{
		users = pusers;
		currentList = pcurrentList;
		
		updateWithoutInputs(updateHidden:false);
		loadFormFieldValues();
		
	}
	
	void loadFormFieldValues() {
		
		//Select Choice values
		this.input__LGLJ4Py1EeWB0q9jTolRAIteratable = this.users;
	}
	
	void ngAfterViewChecked() {
		if(html.querySelector("#form${this.form_UynhcPvuEeWWodZOLky4iQForm.hashCode}")!=null&&!formLoaded) {
			formLoaded = true;
		}
	}
	
	
	/// called after the input variables are loaded, but before the template rendering
	void ngOnInit()
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		
		
		//FORM Field grouping and vaalidation
		this.form_UynhcPvuEeWWodZOLky4iQForm = FormBuilder.controlGroup({
		"input__LGLJ4Py1EeWB0q9jTolRA": [this.users,Validators.compose([
			
		])
		]
		});
		
		//FORM Field loading
		this.loadFormFieldValues();
	}
  
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Add Owner is clicked
  			void actionaddownereventaUytoEPvuEeWWodZOLky4iQEventTrigger(dynamic formValues)
  		
  		{
  			//Submit Form Values
  			this.form_UynhcPvuEeWWodZOLky4iQFormSubmit(formValues);
  			Map<String,dynamic> data = new Map();
  			data['chosenUser'] = this.chosenUser;
  			data['currentList'] = this.currentList;
  			this.actionaddownerevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		DIMEList<todo.ConcreteUser>
  		 initOnDemandusers()
  		{
  			if(this.users==null){
  				this.users = new DIMEList<todo.ConcreteUser>
  				();
  			}
  			return this.users;
  		}
  		void setValueusers(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.users = value;
  		}
  		void userssetValue(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.setValueusers(value);
  		}
  		void usersadd(todo.ConcreteUser
  		 value)
  		{
  			this.users.add(value);
  		}
  		todo.ConcreteUser
  		 initOnDemandchosenUser()
  		{
  			if(this.chosenUser==null){
  				this.chosenUser = new todo.ConcreteUser
  				();
  				this._complex_chosenUser_update.add(this.chosenUser);
  			}
  			return this.chosenUser;
  		}
  		void setValuechosenUser(todo.ConcreteUser
  		 value)
  		{
  			this.chosenUser = value;
  			this._complex_chosenUser_update.add(this.chosenUser);
  		}
  		void chosenUsersetValue(todo.ConcreteUser
  		 value)
  		{
  			this.setValuechosenUser(value);
  		}
  		todo.TODOList
  		 initOnDemandcurrentList()
  		{
  			if(this.currentList==null){
  				this.currentList = new todo.TODOList
  				();
  				this._complex_currentList_update.add(this.currentList);
  			}
  			return this.currentList;
  		}
  		void setValuecurrentList(todo.TODOList
  		 value)
  		{
  			this.currentList = value;
  			this._complex_currentList_update.add(this.currentList);
  		}
  		void currentListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValuecurrentList(value);
  		}
  		
  	
  		
  		
  		
	// Triggered on Form Submit
	void form_UynhcPvuEeWWodZOLky4iQFormSubmit(Map formValues) {
	// Store Form Data in Attributes
	}
		
	
	// methods for the select form field Add Owner
		// dynamic data source for the select from field
		
		/// checks if a given complex selection entry is selected
		bool is_LGLJ4Py1EeWB0q9jTolRASelected(dynamic element)
		{
			if(this.chosenUser == null)return false;
		return element==this.chosenUser;
		}
		
		/// callback to update the variables binded to the selection form component
		void submitSelect_LGLJ4Py1EeWB0q9jTolRABox(dynamic event) {
	// combobox
	int index = int.parse(event.target.selectedOptions[0].value);
	if(index < 0) {
		this.setValuechosenUser(null);
	}
	else {
		this.setValuechosenUser(this.initOnDemandusers()[index]);
	}
		}
}
