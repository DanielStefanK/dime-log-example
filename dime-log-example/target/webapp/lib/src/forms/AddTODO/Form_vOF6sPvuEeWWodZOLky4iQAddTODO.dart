import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services

import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
//GUI plug in component imports
//GUI SIB imports
//Embedded process SIB imports

import 'package:app/src/core/Validators.dart' as validators;

// Form placed in GUI model AddTODO
@Component(
  selector: 'form-_vOF6sPvuEeWWodZOLky4iQ-addTODO',
  viewProviders: const [FORM_BINDINGS],
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  ],
  templateUrl: 'Form_vOF6sPvuEeWWodZOLky4iQAddTODO.html'
)
class Form_vOF6sPvuEeWWodZOLky4iQAddTODO extends dime.DIMEComponent implements OnInit, AfterViewChecked {
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	
  	
  	
  	
  	
  	// branch Add TODO as info.scce.dime.gui.editor.graphiti.api.CButton@5a2e4048 (id: _vOKzMPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@996449c6 (id: _YwQ5kN7gEeW7MO8ocVWj8Q)
  	@Output('actionaddtodoevent') Stream<Map<String,dynamic>> get evt_actionaddtodoevent => actionaddtodoevent.stream;
  	StreamController<Map<String,dynamic>> actionaddtodoevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	
  	//Global Scope
  	//DATA CONTEXT
  	//TODOEntry newEntry
  		@Input()
  		todo.TODOEntry newEntry;
  	//TODOList currentList
  		@Input()
  		todo.TODOList currentList;
  	//Extra input variables
	
	//FORM
	ControlGroup form_vOF6sPvuEeWWodZOLky4iQForm;
	bool formLoaded = false;
	//Form Inputs:
	// input field: Description
	String input__wePT8PvuEeWWodZOLky4iQ;

	
	@Output('complex_currentList_update') Stream<dynamic> get complex_currentList_update => _complex_currentList_update.stream;
	StreamController<dynamic> _complex_currentList_update = new StreamController();
	@Output('complex_newEntry_update') Stream<dynamic> get complex_newEntry_update => _complex_newEntry_update.stream;
	StreamController<dynamic> _complex_newEntry_update = new StreamController();

  	Form_vOF6sPvuEeWWodZOLky4iQAddTODO(DIMEProcessService this.processService,Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		
			  	
		// branch Add TODO declaration
		if(this.actionaddtodoevent!=null) {
			  			this.actionaddtodoevent = new StreamController<Map<String,dynamic>>();  				
		}
			
	}
	
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
		}
		updateImageHash();
	}
	
	void updateInputs(
	{todo.TODOList pcurrentList
	})
	{
		currentList = pcurrentList;
		
		updateWithoutInputs(updateHidden:false);
		loadFormFieldValues();
		
	}
	
	void loadFormFieldValues() {
		
	}
	
	void ngAfterViewChecked() {
		if(html.querySelector("#form${this.form_vOF6sPvuEeWWodZOLky4iQForm.hashCode}")!=null&&!formLoaded) {
			formLoaded = true;
		}
	}
	
	
	/// called after the input variables are loaded, but before the template rendering
	void ngOnInit()
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		
		
		//FORM Field grouping and vaalidation
		this.form_vOF6sPvuEeWWodZOLky4iQForm = FormBuilder.controlGroup({
			"input__wePT8PvuEeWWodZOLky4iQ": [
				""
				,
				Validators.compose([
					
				])
				]
		});
		
		//FORM Field loading
		this.loadFormFieldValues();
	}
  
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Add TODO is clicked
  			void actionaddtodoeventavOKzMPvuEeWWodZOLky4iQEventTrigger(dynamic formValues)
  		
  		{
  			//Submit Form Values
  			this.form_vOF6sPvuEeWWodZOLky4iQFormSubmit(formValues);
  			Map<String,dynamic> data = new Map();
  			data['currentList'] = this.currentList;
  			data['newEntry'] = this.newEntry;
  			this.actionaddtodoevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		todo.TODOEntry
  		 initOnDemandnewEntry()
  		{
  			if(this.newEntry==null){
  				this.newEntry = new todo.TODOEntry
  				();
  				this._complex_newEntry_update.add(this.newEntry);
  			}
  			return this.newEntry;
  		}
  		void setValuenewEntry(todo.TODOEntry
  		 value)
  		{
  			this.newEntry = value;
  			this._complex_newEntry_update.add(this.newEntry);
  		}
  		void newEntrysetValue(todo.TODOEntry
  		 value)
  		{
  			this.setValuenewEntry(value);
  		}
  		todo.TODOList
  		 initOnDemandcurrentList()
  		{
  			if(this.currentList==null){
  				this.currentList = new todo.TODOList
  				();
  				this._complex_currentList_update.add(this.currentList);
  			}
  			return this.currentList;
  		}
  		void setValuecurrentList(todo.TODOList
  		 value)
  		{
  			this.currentList = value;
  			this._complex_currentList_update.add(this.currentList);
  		}
  		void currentListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValuecurrentList(value);
  		}
  		
  	
  		
  		
  		
	// Triggered on Form Submit
	void form_vOF6sPvuEeWWodZOLky4iQFormSubmit(Map formValues) {
	// Store Form Data in Attributes
	// input field Description with type: Text
	if(this.input__wePT8PvuEeWWodZOLky4iQ!=null){
		if(this.input__wePT8PvuEeWWodZOLky4iQ.toString().isNotEmpty){
			this.initOnDemandnewEntry().setValuedescription(
			input__wePT8PvuEeWWodZOLky4iQ
			);
		} else {
			this.initOnDemandnewEntry().setValuedescription('');
		}
	} else {
		this.initOnDemandnewEntry().setValuedescription('');
	}
	}
		
	
}
