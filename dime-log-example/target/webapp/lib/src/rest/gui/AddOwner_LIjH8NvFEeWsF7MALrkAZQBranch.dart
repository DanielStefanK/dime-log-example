// generated by info.scce.dime.generator.gui.rest.DartTOGeneratorHelper#generateGUIOutputsDart

import 'dart:convert';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/core/dime_process_service.dart';

class AddOwner_LIjH8NvFEeWsF7MALrkAZQBranch extends ContinueProcessRequest {
	
	
	Add_Owner branch_Add_Owner;
	
	AddOwner_LIjH8NvFEeWsF7MALrkAZQBranch.forAdd_OwnerBranch(
{		todo.ConcreteUser chosenUser:null,
		todo.TODOList currentList:null
}	) {
		branch_Add_Owner = new Add_Owner();
		branch_Add_Owner.port_chosenUser = chosenUser;
		branch_Add_Owner.port_currentList = currentList;
	}

	String toJSON() {
		return jsonEncode(toJSOG());
	}
		
	Map<String,dynamic> toJSOG()
	{
		Map<String,dynamic> jsonObj = new Map();
		
		if(branch_Add_Owner!=null) {
			return branch_Add_Owner.toJSOG();
		}
		throw new Exception("Exhaustive IF AddOwner_LIjH8NvFEeWsF7MALrkAZQBranch.dart");
	}
}
	
	class Add_Owner {
		
		todo.ConcreteUser port_chosenUser = null;
		todo.TODOList port_currentList = null;

		Map<String,dynamic> toJSOG()
		{
			
			Map<String,dynamic> jsonObj = new Map();
Map<Object,dynamic> cache = new Map();			if(this.port_chosenUser != null){
				jsonObj["chosenUser"] = this.port_chosenUser.toJSOG(cache);
			}
			else{
				jsonObj["chosenUser"] = null;
			}
			if(this.port_currentList != null){
				jsonObj["currentList"] = this.port_currentList.toJSOG(cache);
			}
			else{
				jsonObj["currentList"] = null;
			}
					
			return jsonObj;
		}
		
	}
