import 'dart:async';
import 'dart:html';
import 'package:angular/core.dart';
import 'package:app/src/core/dime_process_service.dart';

class UserHomeConcreteUserx1Service {
    
	Future<String> syncUser() async {
		return (await HttpRequest.getString('${DIMEProcessService.getBaseUrl()}/rest/user/current/ConcreteUser/HomeConcreteUserSelectivex1_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A/private',withCredentials: true));
    }
    
}
