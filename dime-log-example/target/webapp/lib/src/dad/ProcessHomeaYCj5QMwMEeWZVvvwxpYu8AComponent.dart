// routing wrapper for the Home process
import 'package:angular/angular.dart';	
import 'package:angular_router/angular_router.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/FileReference.dart';
//Data
//root process
import 'package:app/src/root/RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponent.dart';


@Component(
  	selector: 'routable-aYCj5QMwMEeWZVvvwxpYu8A-process',
  	directives: const [coreDirectives,routerDirectives,RootProcessaYCj5QMwMEeWZVvvwxpYu8AComponent],
	template: '''
	<root-Home-aYCj5QMwMEeWZVvvwxpYu8A-process
		*ngIf="loaded"
		[startPointId]="startPointId"
	>
	</root-Home-aYCj5QMwMEeWZVvvwxpYu8A-process>
	'''
)
class ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent implements OnActivate, CanReuse {
	
	
	
	bool loaded = false;
	String startPointId = 'aYDBMQMwMEeWZVvvwxpYu8A';
	
	ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent() {}
	
	@override
	void onActivate(_, RouterState current) async {
		loaded = true;
	}
	
	@override
	Future<bool> canReuse(RouterState current, RouterState next) {
	    return Future.value(false);
	}
}

