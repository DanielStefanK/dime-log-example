/*
 * Angular
 */
import 'package:angular/core.dart';
import 'package:angular_router/angular_router.dart';

import 'package:app/src/core/Helper.template.dart' as ng;
import 'package:app/src/core/AbstractRoutes.dart';
import 'package:app/src/core/dime_process_service.dart';
import 'package:app/src/notification/notification_component.dart';
import 'package:app/src/progress-bar/progress_bar_component.dart';
import 'package:app/src/services/TableDndService.dart';
import 'package:app/src/services/ProgressService.dart';
/*
 * Routables
 */
//Route to Home
import 'package:app/src/dad/ProcessHomeaYCj5QMwMEeWZVvvwxpYu8AComponent.template.dart' as ng;

@Component(
    selector: 'app',
    template:'''
    	  <notification #notification></notification>
    	  <progress-bar></progress-bar>
    	  <router-outlet [routes]="Routes.all"></router-outlet>
    ''',
    directives: const [routerDirectives,NotificationComponent,ProgressBarComponent],
    exports: [Routes],
    providers: const [
    	ClassProvider(DIMEProcessService),
    	ClassProvider(NotificationService),
    	ClassProvider(ProgressService), 
		ClassProvider(TableDndService),
        ClassProvider(AbstractRoutes, useClass: Routes),
	]
)
class AppComponent implements OnInit {
		
	@ViewChild('notification')
	NotificationComponent notificationComponent;
	
    final NotificationService _notificationService;
    final ProgressService _progressService;
    
    AppComponent(this._notificationService, this._progressService){}

    @override
	void ngOnInit() async {
    	this._notificationService.component = notificationComponent;
    	print("GENERATED AT 2021/06/28 13:31:03");
	}
}


class Routes implements AbstractRoutes{
	static final root = RouteDefinition(
	    routePath: RoutePath(path: 'home'),
	    component: ng.ProcessaYCj5QMwMEeWZVvvwxpYu8AComponentNgFactory,
	  );
	static final ProcessaBU_fsMwNEeWZVvvwxpYu8AURL = RouteDefinition(
	    routePath: RoutePath(path: 'home'),
	    component: ng.ProcessaYCj5QMwMEeWZVvvwxpYu8AComponentNgFactory,
	  );
  static final Error = RouteDefinition(
    routePath: RoutePath(path: 'error'),
    component: ng.ErrorFoundNgFactory,
  );
  static final Maintenance = RouteDefinition(
  	 routePath: RoutePath(path: 'maintenance'),
  	 component: ng.MaintenanceNgFactory,
  );
  static final NotFound = RouteDefinition(
    path: '.+',
    component: ng.NotFoundNgFactory,
  );

  static final all = <RouteDefinition>[
  	  root,
	  RouteDefinition.redirect(
	    path: '/', redirectTo: ProcessaBU_fsMwNEeWZVvvwxpYu8AURL.toUrl()
	  ),
	  ProcessaBU_fsMwNEeWZVvvwxpYu8AURL,
	  Maintenance,
	  NotFound
  ];

  @override
  RouteDefinition getByName(String typeName) {
  	switch(typeName) {
  		case 'ProcessaBU_fsMwNEeWZVvvwxpYu8AURL': return ProcessaBU_fsMwNEeWZVvvwxpYu8AURL;
		case 'root': return root;
  		case 'Maintenance': return Maintenance;
  		default: return NotFound;
  	}
  }
}
