import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
import 'package:app/src/forms/AddOwner/Form_UynhcPvuEeWWodZOLky4iQAddOwner.dart' as aUynhcPvuEeWWodZOLky4iQ;
//GUI plug in component imports
//GUI SIB imports
//Embedded process SIB imports

import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';

@Component(
  selector: "addowner-tag",
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  aUynhcPvuEeWWodZOLky4iQ.Form_UynhcPvuEeWWodZOLky4iQAddOwner,
  ],
  templateUrl: 'AddOwneraLIjH8NvFEeWsF7MALrkAZQ.html'
)

class AddOwneraLIjH8NvFEeWsF7MALrkAZQ extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
  
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	
  	
  	
  	
  	/// Form
  	@ViewChildren(aUynhcPvuEeWWodZOLky4iQ.Form_UynhcPvuEeWWodZOLky4iQAddOwner)
  	List<aUynhcPvuEeWWodZOLky4iQ.Form_UynhcPvuEeWWodZOLky4iQAddOwner> formComponentaUynhcPvuEeWWodZOLky4iQ;
  	
  	// branch Add Owner as info.scce.dime.gui.editor.graphiti.api.CButton@7ef22a89 (id: _UytoEPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@a59fcabe (id: _LIjH8NvFEeWsF7MALrkAZQ)
  	@Output('actionaddownerevent') Stream<Map<String,dynamic>> get evt_actionaddownerevent => actionaddownerevent.stream;
  	StreamController<Map<String,dynamic>> actionaddownerevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	bool hasToSignIn;
	//DATA CONTEXT
	//ConcreteUser users
		@Input()
		DIMEList<todo.ConcreteUser> users;
	//ConcreteUser chosenUser
		todo.ConcreteUser chosenUser;
	//TODOList currentList
		@Input()
		todo.TODOList currentList;
	
	bool isDestroyed = true;
  
	AddOwneraLIjH8NvFEeWsF7MALrkAZQ(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		this.hasToSignIn = false;
		
		//DATA CONTEXT
		// ConcreteUser users
		this.users = new DIMEList();
		// ConcreteUser chosenUser
		this.chosenUser = null;
		// TODOList currentList
		this.currentList = null;
						
						
		
			  	
		// branch Add Owner declaration
		if(this.actionaddownerevent!=null) {
			  			this.actionaddownerevent = new StreamController<Map<String,dynamic>>();  				
		}
			
		/// Form
		if(formComponentaUynhcPvuEeWWodZOLky4iQ!=null) {
			formComponentaUynhcPvuEeWWodZOLky4iQ.forEach((n) => n.restartComponent());
		}
		updateImageHash();
	}
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
			formComponentaUynhcPvuEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		}
		updateImageHash();
	}
	
	void updateInputs(
	{DIMEList<todo.ConcreteUser>  pusers,
	todo.TODOList pcurrentList
	})
	{
		users = pusers;
		currentList = pcurrentList;
		
		updateWithoutInputs(updateHidden:false);
		formComponentaUynhcPvuEeWWodZOLky4iQ.forEach((n)=>n.updateInputs(
			pusers:users,
			pcurrentList:currentList
		));
	}
	
	
	@override
	void ngOnInit() async
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		this.isDestroyed = false;
		openWebsockets();
	}
	
	void openWebsockets() {
	}
	
	
	@override
	void ngOnDestroy()
	{
	}
	
	
	void ngAfterViewInit() {
		html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
		js.context.callMethod("enableTooltip",[]);
	}
	
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Add Owner is clicked
  		void actionaddownereventaUytoEPvuEeWWodZOLky4iQEventTrigger(Map<String,dynamic> data)
  		{
  			this.actionaddownerevent.add(data);
  		}
  		
  		
  		
  		
  	
  		void formEventactionaddownereventTrigger(Map<String,dynamic> data)
  		{
  			this.actionaddownerevent.add(data);
  		}
  		
  		
  		
  		DIMEList<todo.ConcreteUser>
  		 initOnDemandusers()
  		{
  			if(this.users==null){
  				this.users = new DIMEList<todo.ConcreteUser>
  				();
  			}
  			return this.users;
  		}
  		void setValueusers(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.users = value;
  		}
  		void userssetValue(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.setValueusers(value);
  		}
  		void usersadd(todo.ConcreteUser
  		 value)
  		{
  			this.users.add(value);
  		}
  		todo.ConcreteUser
  		 initOnDemandchosenUser()
  		{
  			if(this.chosenUser==null){
  				this.chosenUser = new todo.ConcreteUser
  				();
  			}
  			return this.chosenUser;
  		}
  		void setValuechosenUser(todo.ConcreteUser
  		 value)
  		{
  			this.chosenUser = value;
  		}
  		void chosenUsersetValue(todo.ConcreteUser
  		 value)
  		{
  			this.setValuechosenUser(value);
  		}
  		todo.TODOList
  		 initOnDemandcurrentList()
  		{
  			if(this.currentList==null){
  				this.currentList = new todo.TODOList
  				();
  			}
  			return this.currentList;
  		}
  		void setValuecurrentList(todo.TODOList
  		 value)
  		{
  			this.currentList = value;
  		}
  		void currentListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValuecurrentList(value);
  		}
  		
  	
  		
  		
  		
  
	/// returns the surrounding container class for major GUI models
	String getContainer_LIjH8NvFEeWsF7MALrkAZQRootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer_LIjH8NvFEeWsF7MALrkAZQId()
	{
		if(this.ismajorpage)return "wrapper";
		return "_LIjH8NvFEeWsF7MALrkAZQ";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer_LIjH8NvFEeWsF7MALrkAZQClass()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect_LIjH8NvFEeWsF7MALrkAZQToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
}
