import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
import 'package:app/src/forms/AddList/Form_ArJncPvvEeWWodZOLky4iQAddList.dart' as aArJncPvvEeWWodZOLky4iQ;
//GUI plug in component imports
//GUI SIB imports
//Embedded process SIB imports

import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';

@Component(
  selector: "addlist-tag",
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  aArJncPvvEeWWodZOLky4iQ.Form_ArJncPvvEeWWodZOLky4iQAddList,
  ],
  templateUrl: 'AddListahGBmcN7nEeW7MO8ocVWj8Q.html'
)

class AddListahGBmcN7nEeW7MO8ocVWj8Q extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
  
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	
  	
  	
  	
  	/// Form
  	@ViewChildren(aArJncPvvEeWWodZOLky4iQ.Form_ArJncPvvEeWWodZOLky4iQAddList)
  	List<aArJncPvvEeWWodZOLky4iQ.Form_ArJncPvvEeWWodZOLky4iQAddList> formComponentaArJncPvvEeWWodZOLky4iQ;
  	
  	// branch Add List as info.scce.dime.gui.editor.graphiti.api.CButton@fdaa9e1f (id: _ArN44PvvEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@6553cc0f (id: _hGBmcN7nEeW7MO8ocVWj8Q)
  	@Output('actionaddlistevent') Stream<Map<String,dynamic>> get evt_actionaddlistevent => actionaddlistevent.stream;
  	StreamController<Map<String,dynamic>> actionaddlistevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	bool hasToSignIn;
	//DATA CONTEXT
	//TODOList newList
		todo.TODOList attr_newList;
	
	bool isDestroyed = true;
  
	AddListahGBmcN7nEeW7MO8ocVWj8Q(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		this.hasToSignIn = false;
		
		//DATA CONTEXT
		// TODOList newList
		this.attr_newList = null;
						
						
		
			  	
		// branch Add List declaration
		if(this.actionaddlistevent!=null) {
			  			this.actionaddlistevent = new StreamController<Map<String,dynamic>>();  				
		}
			
		/// Form
		if(formComponentaArJncPvvEeWWodZOLky4iQ!=null) {
			formComponentaArJncPvvEeWWodZOLky4iQ.forEach((n) => n.restartComponent());
		}
		updateImageHash();
	}
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
			formComponentaArJncPvvEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		}
		updateImageHash();
	}
	
	void updateInputs(
	)
	{
		
		updateWithoutInputs(updateHidden:false);
		formComponentaArJncPvvEeWWodZOLky4iQ.forEach((n)=>n.updateInputs(
		));
	}
	
	
	@override
	void ngOnInit() async
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		this.isDestroyed = false;
		openWebsockets();
	}
	
	void openWebsockets() {
	}
	
	
	@override
	void ngOnDestroy()
	{
	}
	
	
	void ngAfterViewInit() {
		html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
		js.context.callMethod("enableTooltip",[]);
	}
	
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Add List is clicked
  		void actionaddlisteventaArN44PvvEeWWodZOLky4iQEventTrigger(Map<String,dynamic> data)
  		{
  			this.actionaddlistevent.add(data);
  		}
  		
  		
  		
  		
  	
  		void formEventactionaddlisteventTrigger(Map<String,dynamic> data)
  		{
  			this.actionaddlistevent.add(data);
  		}
  		
  		
  		
  		todo.TODOList
  		 initOnDemandattr_newList()
  		{
  			if(this.attr_newList==null){
  				this.attr_newList = new todo.TODOList
  				();
  			}
  			return this.attr_newList;
  		}
  		void setValueattr_newList(todo.TODOList
  		 value)
  		{
  			this.attr_newList = value;
  		}
  		void attr_newListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValueattr_newList(value);
  		}
  		
  	
  		
  		
  		
  
	/// returns the surrounding container class for major GUI models
	String getContainer_hGBmcN7nEeW7MO8ocVWj8QRootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer_hGBmcN7nEeW7MO8ocVWj8QId()
	{
		if(this.ismajorpage)return "wrapper";
		return "_hGBmcN7nEeW7MO8ocVWj8Q";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer_hGBmcN7nEeW7MO8ocVWj8QClass()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect_hGBmcN7nEeW7MO8ocVWj8QToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
}
