import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
import 'package:app/src/tables/ManageOwners/Table_5a2fEPvtEeWWodZOLky4iQManageOwners.dart' as a5a2fEPvtEeWWodZOLky4iQ;
//form component imports
//GUI plug in component imports
//GUI SIB imports
import 'package:app/src/gui/AddOwneraLIjH8NvFEeWsF7MALrkAZQ.dart';
//Embedded process SIB imports

import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';

@Component(
  selector: "manageowners-tag",
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  a5a2fEPvtEeWWodZOLky4iQ.Table_5a2fEPvtEeWWodZOLky4iQManageOwners,
  AddOwneraLIjH8NvFEeWsF7MALrkAZQ,
  ],
  templateUrl: 'ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q.html'
)

class ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
  
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	bool refresh_5a2fEPvtEeWWodZOLky4iQTable;
  	/// GUI AddOwner
  	@ViewChildren(AddOwneraLIjH8NvFEeWsF7MALrkAZQ)
  	List<AddOwneraLIjH8NvFEeWsF7MALrkAZQ> componentSIBahj_HEN7iEeW7MO8ocVWj8Q;
  	
  	
  	
  	
  	
  	/// Table
  	@ViewChildren(a5a2fEPvtEeWWodZOLky4iQ.Table_5a2fEPvtEeWWodZOLky4iQManageOwners)
  	List<a5a2fEPvtEeWWodZOLky4iQ.Table_5a2fEPvtEeWWodZOLky4iQManageOwners> tableComponenta5a2fEPvtEeWWodZOLky4iQ;
  	// branch Add Owner as info.scce.dime.gui.editor.graphiti.api.CButton@7ef22a89 (id: _UytoEPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@a59fcabe (id: _LIjH8NvFEeWsF7MALrkAZQ)
  	@Output('actionaddownerevent') Stream<Map<String,dynamic>> get evt_actionaddownerevent => actionaddownerevent.stream;
  	StreamController<Map<String,dynamic>> actionaddownerevent = new StreamController();
  	// branch Remove Owner as info.scce.dime.gui.editor.graphiti.api.CButton@fb67043e (id: __11lgPvtEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@38b075ba (id: _T77ycN7hEeW7MO8ocVWj8Q)
  	@Output('actionremoveownerevent') Stream<Map<String,dynamic>> get evt_actionremoveownerevent => actionremoveownerevent.stream;
  	StreamController<Map<String,dynamic>> actionremoveownerevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	bool hasToSignIn;
	//DATA CONTEXT
	//ConcreteUser owners
		@Input()
		DIMEList<todo.ConcreteUser> owners;
	//ConcreteUser users
		@Input()
		DIMEList<todo.ConcreteUser> users;
	//TODOList currentList
		@Input()
		todo.TODOList currentList;
	
	bool isDestroyed = true;
  
	ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		this.hasToSignIn = false;
		
		//DATA CONTEXT
		// ConcreteUser owners
		this.owners = new DIMEList();
		// ConcreteUser users
		this.users = new DIMEList();
		// TODOList currentList
		this.currentList = null;
						
						
		
			  	
		// branch Add Owner declaration
		if(this.actionaddownerevent!=null) {
			  			this.actionaddownerevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Remove Owner declaration
		if(this.actionremoveownerevent!=null) {
			  			this.actionremoveownerevent = new StreamController<Map<String,dynamic>>();  				
		}
			
		/// table
		if(tableComponenta5a2fEPvtEeWWodZOLky4iQ!=null) {
			tableComponenta5a2fEPvtEeWWodZOLky4iQ.forEach((n) => n.restartComponent());
		}
		/// GUI AddOwner
		if(componentSIBahj_HEN7iEeW7MO8ocVWj8Q!=null) {
			componentSIBahj_HEN7iEeW7MO8ocVWj8Q.forEach((n)=>n.restartComponent());
		}
		updateImageHash();
	}
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
			tableComponenta5a2fEPvtEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		}
		componentSIBahj_HEN7iEeW7MO8ocVWj8Q.forEach((n)=>n.updateWithoutInputs());
		updateImageHash();
	}
	
	void updateInputs(
	{DIMEList<todo.ConcreteUser>  powners,
	DIMEList<todo.ConcreteUser>  pusers,
	todo.TODOList pcurrentList
	})
	{
		owners = powners;
		users = pusers;
		currentList = pcurrentList;
		
		updateWithoutInputs(updateHidden:false);
		tableComponenta5a2fEPvtEeWWodZOLky4iQ.forEach((n)=>n.updateInputs(
			powners:owners,
			pusers:users,
			pcurrentList:currentList
		));
	}
	
	
	@override
	void ngOnInit() async
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		this.isDestroyed = false;
		openWebsockets();
	}
	
	void openWebsockets() {
	}
	
	
	@override
	void ngOnDestroy()
	{
	}
	
	
	void ngAfterViewInit() {
		html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
		js.context.callMethod("enableTooltip",[]);
	}
	
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Remove Owner is clicked
  		void actionremoveownereventa_11lgPvtEeWWodZOLky4iQEventTrigger(Map<String,dynamic> data)
  		{
  			this.actionremoveownerevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		DIMEList<todo.ConcreteUser>
  		 initOnDemandowners()
  		{
  			if(this.owners==null){
  				this.owners = new DIMEList<todo.ConcreteUser>
  				();
  			}
  			return this.owners;
  		}
  		void setValueowners(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.owners = value;
  		}
  		void ownerssetValue(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.setValueowners(value);
  		}
  		void ownersadd(todo.ConcreteUser
  		 value)
  		{
  			this.owners.add(value);
  		}
  		DIMEList<todo.ConcreteUser>
  		 initOnDemandusers()
  		{
  			if(this.users==null){
  				this.users = new DIMEList<todo.ConcreteUser>
  				();
  			}
  			return this.users;
  		}
  		void setValueusers(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.users = value;
  		}
  		void userssetValue(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.setValueusers(value);
  		}
  		void usersadd(todo.ConcreteUser
  		 value)
  		{
  			this.users.add(value);
  		}
  		todo.TODOList
  		 initOnDemandcurrentList()
  		{
  			if(this.currentList==null){
  				this.currentList = new todo.TODOList
  				();
  			}
  			return this.currentList;
  		}
  		void setValuecurrentList(todo.TODOList
  		 value)
  		{
  			this.currentList = value;
  		}
  		void currentListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValuecurrentList(value);
  		}
  		
  	
  		
  		
  		//GUI SIB AddOwner
  		
  
	/// returns the surrounding container class for major GUI models
	String getContainer_T77ycN7hEeW7MO8ocVWj8QRootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer_T77ycN7hEeW7MO8ocVWj8QId()
	{
		if(this.ismajorpage)return "wrapper";
		return "_T77ycN7hEeW7MO8ocVWj8Q";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer_T77ycN7hEeW7MO8ocVWj8QClass()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect_T77ycN7hEeW7MO8ocVWj8QToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
}
