import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
import 'package:app/src/tables/ManageEntries/Table_zOm0sPvsEeWWodZOLky4iQManageEntries.dart' as azOm0sPvsEeWWodZOLky4iQ;
//form component imports
//GUI plug in component imports
//GUI SIB imports
import 'package:app/src/gui/AddTODOaYwQ5kN7gEeW7MO8ocVWj8Q.dart';
//Embedded process SIB imports

import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';

@Component(
  selector: "manageentries-tag",
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  azOm0sPvsEeWWodZOLky4iQ.Table_zOm0sPvsEeWWodZOLky4iQManageEntries,
  AddTODOaYwQ5kN7gEeW7MO8ocVWj8Q,
  ],
  templateUrl: 'ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q.html'
)

class ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
  
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	// component Default Declaration
  	// table refresh flags
  	bool refresh_zOm0sPvsEeWWodZOLky4iQTable;
  	/// GUI AddTODO
  	@ViewChildren(AddTODOaYwQ5kN7gEeW7MO8ocVWj8Q)
  	List<AddTODOaYwQ5kN7gEeW7MO8ocVWj8Q> componentSIBaxcZI0PvvEeWWodZOLky4iQ;
  	
  	
  	
  	
  	
  	/// Table
  	@ViewChildren(azOm0sPvsEeWWodZOLky4iQ.Table_zOm0sPvsEeWWodZOLky4iQManageEntries)
  	List<azOm0sPvsEeWWodZOLky4iQ.Table_zOm0sPvsEeWWodZOLky4iQManageEntries> tableComponentazOm0sPvsEeWWodZOLky4iQ;
  	// branch Add TODO as info.scce.dime.gui.editor.graphiti.api.CButton@5a2e4048 (id: _vOKzMPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@996449c6 (id: _YwQ5kN7gEeW7MO8ocVWj8Q)
  	@Output('actionaddtodoevent') Stream<Map<String,dynamic>> get evt_actionaddtodoevent => actionaddtodoevent.stream;
  	StreamController<Map<String,dynamic>> actionaddtodoevent = new StreamController();
  	// branch Delete TODO as info.scce.dime.gui.editor.graphiti.api.CButton@fcc780ce (id: _9IttYPvsEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@744a8e20 (id: _bS1KsN7lEeW7MO8ocVWj8Q)
  	@Output('actiondeletetodoevent') Stream<Map<String,dynamic>> get evt_actiondeletetodoevent => actiondeletetodoevent.stream;
  	StreamController<Map<String,dynamic>> actiondeletetodoevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	bool hasToSignIn;
	//DATA CONTEXT
	//TODOList currentList
		@Input()
		todo.TODOList currentList;
	
	bool isDestroyed = true;
  
	ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		this.hasToSignIn = false;
		
		//DATA CONTEXT
		// TODOList currentList
		this.currentList = null;
						
						
		
			  	
		// branch Add TODO declaration
		if(this.actionaddtodoevent!=null) {
			  			this.actionaddtodoevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Delete TODO declaration
		if(this.actiondeletetodoevent!=null) {
			  			this.actiondeletetodoevent = new StreamController<Map<String,dynamic>>();  				
		}
			
		/// table
		if(tableComponentazOm0sPvsEeWWodZOLky4iQ!=null) {
			tableComponentazOm0sPvsEeWWodZOLky4iQ.forEach((n) => n.restartComponent());
		}
		/// GUI AddTODO
		if(componentSIBaxcZI0PvvEeWWodZOLky4iQ!=null) {
			componentSIBaxcZI0PvvEeWWodZOLky4iQ.forEach((n)=>n.restartComponent());
		}
		updateImageHash();
	}
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
			tableComponentazOm0sPvsEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		}
		componentSIBaxcZI0PvvEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		updateImageHash();
	}
	
	void updateInputs(
	{todo.TODOList pcurrentList
	})
	{
		currentList = pcurrentList;
		
		updateWithoutInputs(updateHidden:false);
		tableComponentazOm0sPvsEeWWodZOLky4iQ.forEach((n)=>n.updateInputs(
			pcurrentList:currentList
		));
	}
	
	
	@override
	void ngOnInit() async
	{
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		this.isDestroyed = false;
		openWebsockets();
	}
	
	void openWebsockets() {
	}
	
	
	@override
	void ngOnDestroy()
	{
	}
	
	
	void ngAfterViewInit() {
		html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
		js.context.callMethod("enableTooltip",[]);
	}
	
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Delete TODO is clicked
  		void actiondeletetodoeventa9IttYPvsEeWWodZOLky4iQEventTrigger(Map<String,dynamic> data)
  		{
  			this.actiondeletetodoevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		todo.TODOList
  		 initOnDemandcurrentList()
  		{
  			if(this.currentList==null){
  				this.currentList = new todo.TODOList
  				();
  			}
  			return this.currentList;
  		}
  		void setValuecurrentList(todo.TODOList
  		 value)
  		{
  			this.currentList = value;
  		}
  		void currentListsetValue(todo.TODOList
  		 value)
  		{
  			this.setValuecurrentList(value);
  		}
  		
  	
  		
  		
  		//GUI SIB AddTODO
  		
  
	/// returns the surrounding container class for major GUI models
	String getContainer_bS1KsN7lEeW7MO8ocVWj8QRootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer_bS1KsN7lEeW7MO8ocVWj8QId()
	{
		if(this.ismajorpage)return "wrapper";
		return "_bS1KsN7lEeW7MO8ocVWj8Q";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer_bS1KsN7lEeW7MO8ocVWj8QClass()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect_bS1KsN7lEeW7MO8ocVWj8QToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
}
