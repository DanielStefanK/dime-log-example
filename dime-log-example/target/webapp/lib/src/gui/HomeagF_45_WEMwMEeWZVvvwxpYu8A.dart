import 'dart:async';
import 'dart:convert';
import 'dart:html' as html;

import 'package:angular_router/angular_router.dart';
import 'package:angular/angular.dart';
import 'package:angular/security.dart';
import 'package:angular_forms/angular_forms.dart';

import 'package:app/src/app.dart';

import 'package:app/src/core/dime_process_service.dart';
//Notifications
import 'package:app/src/notification/notification_component.dart';
//Login
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/core/DIMEComponent.dart' as dime;

//Data
import 'package:app/src/data/todo.dart' as todo;
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/filesupport/fileuploader.dart';
import 'package:app/src/filesupport/fileselect.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/models/Todos.dart';
//Directives
import 'package:app/src/directives/MaxLength.dart';
import 'package:app/src/directives/DimeCustomeAttributes.dart';

//Import Services
import 'package:app/src/services/UserHomeConcreteUserx1Service.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:markdown/markdown.dart' as markdown;
import 'package:app/src/modal/Modal.dart' as modal;
//Panel
import 'package:app/src/panel/Panel.dart' as panel;

//file guard imports
//special element imports
//table component imports
//form component imports
//GUI plug in component imports
//GUI SIB imports
import 'package:app/src/gui/AddListahGBmcN7nEeW7MO8ocVWj8Q.dart';
import 'package:app/src/gui/ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q.dart';
import 'package:app/src/gui/ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q.dart';
//Embedded process SIB imports

import 'dart:js' as js;
import 'package:app/src/core/AbstractRoutes.dart';

@Component(
  selector: "home-tag",
  providers: const [ClassProvider(UserHomeConcreteUserx1Service)],
  pipes: const [commonPipes,SecondsPipe],
  encapsulation: ViewEncapsulation.None,
  exports: const [
  	todo.TodoCastUtil
  ],
  directives: const [
  coreDirectives,panel.Panel,modal.Modal,DimeCustomeAttributes,formDirectives,FileSelect,routerDirectives,MaxLength,login.Login,SafeInnerHtmlDirective,
  AddListahGBmcN7nEeW7MO8ocVWj8Q,
  ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q,
  ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q,
  ],
  templateUrl: 'HomeagF_45_WEMwMEeWZVvvwxpYu8A.html'
)

class HomeagF_45_WEMwMEeWZVvvwxpYu8A extends dime.DIMEComponent implements OnInit, OnDestroy, AfterViewInit {
  
  	// common declarations
  	bool refresh;
  	@Input()
  	bool ismajorpage = false;
  	@Input()
  	String currentbranch;
  	@Input()
  	bool modalDialog = false;
  	ChangeDetectorRef cdr;
  	@Input()
  	String runtimeId;
  	@Input()
  	String guiId;
  	final Router router;
  	final DIMEProcessService processService;
  	final DomSanitizationService domSanitizationService; 
  	UserHomeConcreteUserx1Service userHomeConcreteUserx1Service;
  	// component Default Declaration
  	// table refresh flags
  	/// GUI AddList
  	@ViewChildren(AddListahGBmcN7nEeW7MO8ocVWj8Q)
  	List<AddListahGBmcN7nEeW7MO8ocVWj8Q> componentSIBaQZX7MPvsEeWWodZOLky4iQ;
  	/// GUI ManageOwners
  	@ViewChildren(ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q)
  	List<ManageOwnersaT77ycN7hEeW7MO8ocVWj8Q> componentSIBaUXnSYPvsEeWWodZOLky4iQ;
  	/// GUI ManageEntries
  	@ViewChildren(ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q)
  	List<ManageEntriesabS1KsN7lEeW7MO8ocVWj8Q> componentSIBaTiQXsPvsEeWWodZOLky4iQ;
  	
  	
  	
  	
  	
  	// branch Add List as info.scce.dime.gui.editor.graphiti.api.CButton@fdaa9e1f (id: _ArN44PvvEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@6553cc0f (id: _hGBmcN7nEeW7MO8ocVWj8Q)
  	@Output('actionaddlistevent') Stream<Map<String,dynamic>> get evt_actionaddlistevent => actionaddlistevent.stream;
  	StreamController<Map<String,dynamic>> actionaddlistevent = new StreamController();
  	// branch Add Owner as info.scce.dime.gui.editor.graphiti.api.CButton@7ef22a89 (id: _UytoEPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@a59fcabe (id: _LIjH8NvFEeWsF7MALrkAZQ)
  	@Output('actionaddownerevent') Stream<Map<String,dynamic>> get evt_actionaddownerevent => actionaddownerevent.stream;
  	StreamController<Map<String,dynamic>> actionaddownerevent = new StreamController();
  	// branch Remove Owner as info.scce.dime.gui.editor.graphiti.api.CButton@fb67043e (id: __11lgPvtEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@38b075ba (id: _T77ycN7hEeW7MO8ocVWj8Q)
  	@Output('actionremoveownerevent') Stream<Map<String,dynamic>> get evt_actionremoveownerevent => actionremoveownerevent.stream;
  	StreamController<Map<String,dynamic>> actionremoveownerevent = new StreamController();
  	// branch Add TODO as info.scce.dime.gui.editor.graphiti.api.CButton@5a2e4048 (id: _vOKzMPvuEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@996449c6 (id: _YwQ5kN7gEeW7MO8ocVWj8Q)
  	@Output('actionaddtodoevent') Stream<Map<String,dynamic>> get evt_actionaddtodoevent => actionaddtodoevent.stream;
  	StreamController<Map<String,dynamic>> actionaddtodoevent = new StreamController();
  	// branch Delete TODO as info.scce.dime.gui.editor.graphiti.api.CButton@fcc780ce (id: _9IttYPvsEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@744a8e20 (id: _bS1KsN7lEeW7MO8ocVWj8Q)
  	@Output('actiondeletetodoevent') Stream<Map<String,dynamic>> get evt_actiondeletetodoevent => actiondeletetodoevent.stream;
  	StreamController<Map<String,dynamic>> actiondeletetodoevent = new StreamController();
  	// branch Remove as info.scce.dime.gui.editor.graphiti.api.CButton@12087fd4 (id: _CqcGAPvsEeWWodZOLky4iQ) in info.scce.dime.gui.editor.graphiti.api.CGUI@7caeb8b (id: _gF-WEMwMEeWZVvvwxpYu8A)
  	@Output('actionremoveevent') Stream<Map<String,dynamic>> get evt_actionremoveevent => actionremoveevent.stream;
  	StreamController<Map<String,dynamic>> actionremoveevent = new StreamController();
  	
  	@ViewChildren(modal.Modal)
  	List<modal.Modal> modals;
  	bool hasToSignIn;
	bool showLogin = false;
	//DATA CONTEXT
	//ConcreteUser currentUser
		todo.ConcreteUser currentUser;
	//ConcreteUser allUsers
		@Input()
		DIMEList<todo.ConcreteUser> allUsers;
	
	bool isDestroyed = true;
  
	HomeagF_45_WEMwMEeWZVvvwxpYu8A(DIMEProcessService this.processService, Router this.router,DomSanitizationService this.domSanitizationService,AbstractRoutes routes,UserHomeConcreteUserx1Service this.userHomeConcreteUserx1Service
	) : super(domSanitizationService,processService,routes)
	{
		restartComponent();
	}
	
	void restartComponent() {
		
		this.hasToSignIn = false;
		
		//DATA CONTEXT
		// ConcreteUser currentUser
		this.currentUser = null;
		// ConcreteUser allUsers
		this.allUsers = new DIMEList();
						
						
		
			  	
		// branch Add List declaration
		if(this.actionaddlistevent!=null) {
			  			this.actionaddlistevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Add Owner declaration
		if(this.actionaddownerevent!=null) {
			  			this.actionaddownerevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Remove Owner declaration
		if(this.actionremoveownerevent!=null) {
			  			this.actionremoveownerevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Add TODO declaration
		if(this.actionaddtodoevent!=null) {
			  			this.actionaddtodoevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Delete TODO declaration
		if(this.actiondeletetodoevent!=null) {
			  			this.actiondeletetodoevent = new StreamController<Map<String,dynamic>>();  				
		}
		// branch Remove declaration
		if(this.actionremoveevent!=null) {
			  			this.actionremoveevent = new StreamController<Map<String,dynamic>>();  				
		}
			
		/// GUI AddList
		if(componentSIBaQZX7MPvsEeWWodZOLky4iQ!=null) {
			componentSIBaQZX7MPvsEeWWodZOLky4iQ.forEach((n)=>n.restartComponent());
		}
		/// GUI ManageOwners
		if(componentSIBaUXnSYPvsEeWWodZOLky4iQ!=null) {
			componentSIBaUXnSYPvsEeWWodZOLky4iQ.forEach((n)=>n.restartComponent());
		}
		/// GUI ManageEntries
		if(componentSIBaTiQXsPvsEeWWodZOLky4iQ!=null) {
			componentSIBaTiQXsPvsEeWWodZOLky4iQ.forEach((n)=>n.restartComponent());
		}
		updateImageHash();
	}
	
	void updateWithoutInputs({bool updateHidden:true}) {
		modals.forEach((m)=>m.close());
		if(updateHidden) {
		}
		componentSIBaQZX7MPvsEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		componentSIBaUXnSYPvsEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		componentSIBaTiQXsPvsEeWWodZOLky4iQ.forEach((n)=>n.updateWithoutInputs());
		updateImageHash();
	}
	
	void updateInputs(
	{DIMEList<todo.ConcreteUser>  pallUsers
	})
	{
		allUsers = pallUsers;
		
		updateWithoutInputs(updateHidden:false);
		this.loadCurrentUser();
	}
	
	
	@override
	void ngOnInit() async
	{
		this.loadCurrentUser();
		initializeDateFormatting(html.window.navigator.language,null).then((_)=>Intl.defaultLocale = html.window.navigator.language);
		
		
		this.isDestroyed = false;
		openWebsockets();
	}
	
	void openWebsockets() {
	}
	
	
	@override
	void ngOnDestroy()
	{
	}
	
	
	void ngAfterViewInit() {
		html.window.document.dispatchEvent(new html.CustomEvent('dime-component-ready'));
		js.context.callMethod("enableTooltip",[]);
	}
	
  		@override
  		String getRuntimeId() => this.runtimeId;
  		
  		
  		
  		
  	
  		/// callback, if the button Remove is clicked
  			void actionremoveeventaCqcGAPvsEeWWodZOLky4iQEventTrigger(
  				p_SvNF4NDFEeWvl7dmsyHdTA
  				
  				)
  		
  		{
  			Map<String,dynamic> data = new Map();
  			data['currentList'] = p_SvNF4NDFEeWvl7dmsyHdTA;
  			this.actionremoveevent.add(data);
  		}
  		
  		
  		
  		
  	
  		
  		
  		
  		todo.ConcreteUser
  		 initOnDemandcurrentUser()
  		{
  			if(this.currentUser==null){
  				this.currentUser = new todo.ConcreteUser
  				();
  			}
  			return this.currentUser;
  		}
  		void setValuecurrentUser(todo.ConcreteUser
  		 value)
  		{
  			this.currentUser = value;
  		}
  		void currentUsersetValue(todo.ConcreteUser
  		 value)
  		{
  			this.setValuecurrentUser(value);
  		}
  		DIMEList<todo.ConcreteUser>
  		 initOnDemandallUsers()
  		{
  			if(this.allUsers==null){
  				this.allUsers = new DIMEList<todo.ConcreteUser>
  				();
  			}
  			return this.allUsers;
  		}
  		void setValueallUsers(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.allUsers = value;
  		}
  		void allUserssetValue(DIMEList<todo.ConcreteUser>
  		 value)
  		{
  			this.setValueallUsers(value);
  		}
  		void allUsersadd(todo.ConcreteUser
  		 value)
  		{
  			this.allUsers.add(value);
  		}
  		
  	
  		
  		
  		//GUI SIB AddList
  		//GUI SIB ManageOwners
  		//GUI SIB ManageEntries
  		
  
  	void loadCurrentUser()
  	{
  		this.userHomeConcreteUserx1Service.syncUser().then((value){
  			this.currentUser = todo.ConcreteUser.fromJSON(value);
  			this.showLogin = false;
  			updateImageHash();
  			openWebsockets();
  			
  		}).catchError((error){
  			if(error.currentTarget.status != 200){
  				this.showLogin = true;
  			}
  		});
  	}
	/// returns the surrounding container class for major GUI models
	String getContainer_gFWEMwMEeWZVvvwxpYu8ARootClass()
	{
		if(this.ismajorpage)return "";
		return "";
	}
	
	/// returns the surrounding wrapper class for major GUI models
	String getContainer_gFWEMwMEeWZVvvwxpYu8AId()
	{
		if(this.ismajorpage)return "wrapper";
		return "_gFWEMwMEeWZVvvwxpYu8A";
	}
	
	/// returns the surrounding container class for major GUI models
	String getContainer_gFWEMwMEeWZVvvwxpYu8AClass()
	{
		if(this.ismajorpage)return "container-display";
		return "";
	}
	
	/// callback, to go back to the root interaction
	void redirect_gFWEMwMEeWZVvvwxpYu8AToHome(dynamic e)
	{
		e.preventDefault();
		this.router.navigate(Routes.root.toUrl());
	}
}
