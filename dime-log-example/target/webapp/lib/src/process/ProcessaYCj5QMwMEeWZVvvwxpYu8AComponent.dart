// root Home process combines all front end sibs reachable
import 'package:angular/angular.dart';	
import 'package:angular_router/angular_router.dart';
import 'dart:async';
import 'package:app/src/core/dime_process_service.dart';
import 'package:app/src/core/AbstractRoutes.dart';
import 'package:app/src/login/Login.dart' as login;
import 'package:app/src/notification/notification_component.dart';
//routes
import 'package:app/src/app.dart' as main;
//Data
import 'package:app/src/models/FileReference.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:app/src/data/todo.dart' as todo;
//GUI model outputs
import 'package:app/src/rest/gui/Home_gF_45_WEMwMEeWZVvvwxpYu8AInput.dart';
import 'package:app/src/rest/gui/Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.dart';
import 'package:app/src/gui/HomeagF_45_WEMwMEeWZVvvwxpYu8A.dart';
//NF model outputs

@Component(
  	selector: 'Home-aYCj5QMwMEeWZVvvwxpYu8A-process',
  	directives: const [
  		coreDirectives,login.Login,HomeagF_45_WEMwMEeWZVvvwxpYu8A
  	],
	template: '''
	<login-form 
		*ngIf="showLogin"
		(signedin)="retry()"
		[modal]="false"
	></login-form>
	<template [ngIf]="!showLogin">
		<!-- Process Home -->
		<home-tag
			*ngIf="isVisible(majorSIB,'_zEoYcMwMEeWZVvvwxpYu8A')"
			[allUsers]="majorazEoYcMwMEeWZVvvwxpYu8AInput.allUsers"
			[guiId]="'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A'"
			[runtimeId]="runtimeId"
			[currentbranch]="currentBranch"
			[modalDialog]="false"
			[ismajorpage]="true"
			(actionaddtodoevent) = "eventaFThDANMZEeWJV9QOcxWQYgAdd_TODOTrigger(\$event)"
			(actionaddownerevent) = "eventaFTyIwNMZEeWJV9QOcxWQYgAdd_OwnerTrigger(\$event)"
			(actiondeletetodoevent) = "eventajjWXUNMcEeWJV9QOcxWQYgDelete_TODOTrigger(\$event)"
			(actionremoveownerevent) = "eventajjYMgNMcEeWJV9QOcxWQYgRemove_OwnerTrigger(\$event)"
			(actionaddlistevent) = "eventaGwCukNMdEeWJV9QOcxWQYgAdd_ListTrigger(\$event)"
			(actionremoveevent) = "eventasOBOwd8MEeWlD7knc3NOdQRemoveTrigger(\$event)"
		>
		</home-tag>
	</template>
	'''
)
class ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent extends GUIProcess implements OnInit, OnChanges, AfterViewChecked {
	
	@Input()
	String runtimeId;
	
	@Input()
	String parentRuntimeId;
	
	@Input()
	String majorSIB;
	
	@Input()
	String minorSIB;
	
	@Input()
	String guiId;
	
	@Input()
	String sibId;
	
	@Input()
	UserInteractionResponse majorInput;
	
	@Input()
	UserInteractionResponse minorInput;
	
	@Input()
	UserInteractionResponseDeserializer deserializer;
	
	@override
	UserInteractionResponseDeserializer getDeserializer() => deserializer;
	
	Home_gF_45_WEMwMEeWZVvvwxpYu8AInput majorazEoYcMwMEeWZVvvwxpYu8AInput;
	Home_gF_45_WEMwMEeWZVvvwxpYu8AInput minorazEoYcMwMEeWZVvvwxpYu8AInput;
	
	final DIMEProcessService _processService;
	
	final Router _router;
	
	final NotificationService _notificationService;
	
	
	// GUISIBs of Process Home
	// GUISIB Home
	@ViewChildren(HomeagF_45_WEMwMEeWZVvvwxpYu8A)
	List<HomeagF_45_WEMwMEeWZVvvwxpYu8A> componentazEoYcMwMEeWZVvvwxpYu8A;
	
	ProcessaYCj5QMwMEeWZVvvwxpYu8AComponent(this._processService,this._router,this._notificationService,AbstractRoutes routes): super(routes);

	@override
	ngOnInit() {
		reActivateProcess();
		started = true;
	}
	
	@override
	ngAfterViewChecked() {
	}
	
	void retry() {
		retryAfterLogin(_processService,"Home_YCj5QMwMEeWZVvvwxpYu8A",sibId:sibId);
	}
	
	@override
	void ngOnChanges(Map<String, SimpleChange> changes) {
		reActivateProcess();
	}
	
	@override
	Map<String,ActiveProcess> getActiveProcesses() => _processService.activeProcesses;
	
	@override
	String getParentRuntimeId() => parentRuntimeId;
	
	@override
	String getRuntimeId() => runtimeId;
	
	@override
    String getGUIId() => guiId;
    
    @override
    Router getRouter() => _router;
    
    @override
    NotificationService getNotificationService() => _notificationService;
	
	/// starts a front end routine or a GUI by event
	void reActivateProcess() {
		
		if(isVisible(majorSIB,'_zEoYcMwMEeWZVvvwxpYu8A')) {
			majorazEoYcMwMEeWZVvvwxpYu8AInput = majorInput as Home_gF_45_WEMwMEeWZVvvwxpYu8AInput;
			if(componentazEoYcMwMEeWZVvvwxpYu8A != null) {
				componentazEoYcMwMEeWZVvvwxpYu8A.forEach((n)=>n.updateInputs(
					pallUsers:majorazEoYcMwMEeWZVvvwxpYu8AInput.allUsers
				));
			}
		} else if(isVisible(minorSIB,'_zEoYcMwMEeWZVvvwxpYu8A')) {
			Home_gF_45_WEMwMEeWZVvvwxpYu8AInput newInput = minorInput as Home_gF_45_WEMwMEeWZVvvwxpYu8AInput;
			bool hasChanged = minorazEoYcMwMEeWZVvvwxpYu8AInput!=null?minorazEoYcMwMEeWZVvvwxpYu8AInput.inpusChanged(newInput):true;
			minorazEoYcMwMEeWZVvvwxpYu8AInput = newInput;
			if(componentazEoYcMwMEeWZVvvwxpYu8A != null) {
				componentazEoYcMwMEeWZVvvwxpYu8A.forEach((n)=>n.updateInputs(
					pallUsers:minorazEoYcMwMEeWZVvvwxpYu8AInput.allUsers
				));
				if(hasChanged) {
					componentazEoYcMwMEeWZVvvwxpYu8A.forEach((n)=>n.restartComponent());			
				}
			}
		}
		
		
	}
	// Branches for GUISIB Home of Process Home
	// Branch Add TODO
	void eventaFThDANMZEeWJV9QOcxWQYgAdd_TODOTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forAdd_TODOBranch(
			currentList:map['currentList'] as todo.TODOList,
			newEntry:map['newEntry'] as todo.TODOEntry
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Add_TODO/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
	// Branch Add Owner
	void eventaFTyIwNMZEeWJV9QOcxWQYgAdd_OwnerTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forAdd_OwnerBranch(
			chosenUser:map['chosenUser'] as todo.ConcreteUser,
			currentList:map['currentList'] as todo.TODOList
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Add_Owner/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
	// Branch Delete TODO
	void eventajjWXUNMcEeWJV9QOcxWQYgDelete_TODOTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forDelete_TODOBranch(
			currentEntry:map['currentEntry'] as todo.TODOEntry,
			currentList:map['currentList'] as todo.TODOList
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Delete_TODO/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
	// Branch Remove Owner
	void eventajjYMgNMcEeWJV9QOcxWQYgRemove_OwnerTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forRemove_OwnerBranch(
			currentOwner:map['currentOwner'] as todo.ConcreteUser,
			currentList:map['currentList'] as todo.TODOList
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Remove_Owner/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
	// Branch Add List
	void eventaGwCukNMdEeWJV9QOcxWQYgAdd_ListTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forAdd_ListBranch(
			attr_newList:map['newList'] as todo.TODOList
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Add_List/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
	// Branch Remove
	void eventasOBOwd8MEeWlD7knc3NOdQRemoveTrigger(Map<String,dynamic> map)
	{
		var result = new Home_gF_45_WEMwMEeWZVvvwxpYu8ABranch.forRemoveBranch(
			currentList:map['currentList'] as todo.TODOList
		);
		_processService.continueProcess(
			deserializer,
			getActiveProcesses()[runtimeId].runtime,
			getRuntimeId(),
			'_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A',
			'Remove/branch/public',
			result.toJSOG(),
			parentRuntimeId:getParentRuntimeId()
		)
		.then((cpr)=>processResponse(_processService,cpr))
		.catchError((e)=>processError(e));
	}
}
