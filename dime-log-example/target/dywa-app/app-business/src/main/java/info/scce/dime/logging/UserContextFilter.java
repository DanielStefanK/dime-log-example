package info.scce.dime.logging;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.DispatcherType;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebFilter;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import org.slf4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Servlet filter for adding the user data to the logging context
 *
 * @author danielstefank
 */

@WebFilter(
  filterName = "UserContextFilter",
  urlPatterns = {"/rest/**"},
  dispatcherTypes = {DispatcherType.REQUEST}
)
public class UserContextFilter
  implements Filter {

  /**
   * Logger for this class
   */

  private Logger LOGGER = LoggerFactory.getLogger(UserContextFilter.class);


  /**
   * Init function the initializing the filter. Nothing to do here for this filter
   *
   * @param fConfig the filter config from the web.xml
   */

  public void init(FilterConfig fConfig) {
    // nothing to do
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException {
    try {
      Subject currentUser = SecurityUtils.getSubject();

      if (currentUser.isAuthenticated()) {
        Long userId = (Long) currentUser.getPrincipal();
        MDC.put("USER_ID", userId.toString());
      }
    } catch (IllegalStateException ex) {
      // no user defined for request public/health request
      LOGGER.info("No subject defined for this request");
    }

    // go on with the request
    chain.doFilter(request, response);
  }


  /**
   * Destroy life cycle hook. Does nothing for this filter
   */

  public void destroy() {
    //nothing to do
  }

}
