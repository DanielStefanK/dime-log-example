package info.scce.dime.logging;


import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Class that handles the error logs created in the frontend
 *
 * @author danielstefank
 */

public class FrontendErrorRequest {

  /** list of all logs that occurred */

  private List<Log> logs;


  /**
   * Set the logs
   *
   * @return log to be set
   */

  @JsonProperty("logs")
  public List<Log> getLogs() {
    return logs;
  }


  /**
   * Get the logs that occured
   *
   * @param logs the logs
   */

  @JsonProperty("logs")
  public void setLogs(List<Log> logs) {
    this.logs = logs;
  }


  
  /**
   * Class representing on log
   */

  public static class Log {
    /** the timestamp */

    private Date timestamp;


    /** the log itself*/

    private String log;


    /** the windowHeight meta data  */

    private String windowHeight;


    /** the windowWidth meta data  */

    private String windowWidth;


    /** the borwserVersion meta data  */

    private String browserVersion;


    /** the fullUrl meta data  */

    private String fullUrl;


    /**
     * Constructor
     */

    public Log() {}


    /**
     * Get the timestamp the time the error was issued in the frontend
     *
     * @return the timestamp of the log
     */

    @JsonProperty("timestamp")
    public Date getTimestamp() {
      return timestamp;
    }


    /**
     * Set the timestamp the time the error was issued in the frontend
     *
     * @param timestamp  the time stamp
     */

    @JsonProperty("timestamp")
    public void setTimestamp(Date timestamp) {
      this.timestamp = timestamp;
    }

    /**
     * Get the log message that was issued in the frontend
     *
     * @return the log message
     */

    @JsonProperty("log")
    public String getLog() {
      return log;
    }


    /**
     * Set the log log message that was issued in the frontend
     *
     * @param log the log message to be set
     */

    @JsonProperty("log")
    public void setLog(String log) {
      this.log = log;
    }


    /**
     * Get the window height meta data
     *
     * @return the window height
     */

    @JsonProperty("windowHeight")
    public String getWindowHeight() {
      return windowHeight;
    }


    /**
     * Set the window height meta data
     *
     * @param windowHeight  the window height to be set
     */

    @JsonProperty("windowHeight")
    public void setWindowHeight(String windowHeight) {
      this.windowHeight = windowHeight;
    }


    /**
     * Get the window width meta data
     *
     * @return the window width
     */

    @JsonProperty("windowWidth")
    public String getWindowWidth() {
      return windowWidth;
    }


    /**
     * Set the window width meta data
     *
     * @param windowWidth the window width meta data to be set
     */

    @JsonProperty("windowWidth")
    public void setWindowWidth(String windowWidth) {
      this.windowWidth = windowWidth;
    }

    /**
     * Get the browser Version
     *
     * @return the browser version
     */

    @JsonProperty("browserVersion")
    public String getBrowserVersion() {
      return browserVersion;
    }


    /**
     * Set the browser bersion
     *
     * @param browserVersion  version to be set
     */

    @JsonProperty("browserVersion")
    public void setBrowserVersion(String browserVersion) {
      this.browserVersion = browserVersion;
    }


    /**
     * Get the full url when the error occurred
     *
     * @return the full url
     */

    @JsonProperty("fullUrl")
    public String getFullUrl() {
      return fullUrl;
    }


    /**
     * Set the full url
     *
     * @param fullUrl url to be set
     */

    @JsonProperty("fullUrl")
    public void setFullUrl(String fullUrl) {
      this.fullUrl = fullUrl;
    }


    /**
     * Print the contents of this object for logging purposes
     *
     * @return the string representation
     */

    public String toString() {
      StringBuilder sb = new StringBuilder("Browser Meta Data = [");

      sb.append("width = ");
      sb.append(windowWidth);
      sb.append(", height = ");
      sb.append(windowHeight);
      sb.append(", url = ");
      sb.append(fullUrl);
      sb.append(", browserVersion = ");
      sb.append(browserVersion);
      sb.append(", timestamp = ");
      sb.append(timestamp);
      sb.append(", log = ");
      sb.append(log);

      sb.append("]");
      return sb.toString();
    }
  }
}
