package info.scce.dime.logging;


import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.DispatcherType;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebFilter;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@WebFilter(
		filterName = "ResponseInterceptorFilter",
		urlPatterns = "*",
		dispatcherTypes = {DispatcherType.REQUEST}
)
public class ResponseInterceptorFilter
	implements Filter {

	/**
	 * Logger for this class
	 */

	private Logger LOGGER = LoggerFactory.getLogger(ResponseInterceptorFilter.class);


	/**
	 * Init function the initializing the filter. Destroy life cycle hook.
	 *
	 * @param fConfig               the filter config from the web.xml
	 */

	public void init(FilterConfig fConfig) {
		// nothing to do here
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {
		// get start time
		Instant start = Instant.now();
		// handle the request
		chain.doFilter(request, response);
		// get end time
		Instant finish = Instant.now();

		long timeElapsed = Duration.between(start, finish).toMillis();

		// pass the request along the filter chain
		if (response instanceof HttpServletResponse) {
			HttpServletResponse res = (HttpServletResponse) response;
			// only log error responses (CODE: 5xx)
			if (res.getStatus() % 500 < 100){
				LOGGER.error("Error Response Code: {}", res.getStatus());
			}

			// only log error responses (CODE: 4xx)
			if (res.getStatus() % 400 < 100){
				LOGGER.warn("Error Response Code: {}", res.getStatus());
			}
		}

		LOGGER.info("Response finished. Duration={}ms", timeElapsed);
	}


	/**
	 * Destroy life cycle hook. Does nothing for this filter
	 */

	public void destroy() {
		// nothing to do here
	}

}
