package info.scce.dime.logging;


import java.io.IOException;
import java.util.UUID;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import javax.servlet.annotation.WebFilter;

import javax.servlet.http.HttpServletRequest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Filter for logging the request that was received and adding request specific context to the log
 *
 * @author danielstefank
 */

@WebFilter(
  filterName = "LoggingContextFilter",
  urlPatterns = "*",
  dispatcherTypes = {DispatcherType.REQUEST}
)
public class LoggingContextFilter implements Filter {

  /**
   * Logger for this class
   */

  private Logger LOGGER = LoggerFactory.getLogger(LoggingContextFilter.class);


  /** the transaction ID key */

  public static String TXN_ID_KEY = "TXN_ID";


  /**
   * Init function the initializing the filter. Nothing to do here for this filter
   *
   * @param fConfig the filter config from the web.xml
   */

  public void init(FilterConfig fConfig) {
    // nothing to do
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException {
    // set a unique transaction id to the Logging context
    String transactionId = UUID.randomUUID().toString();
    MDC.put(TXN_ID_KEY, transactionId);

    // add request specific data to logging context
    if (request instanceof HttpServletRequest) {
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      MDC.put("REQUESTED_URI", httpRequest.getRequestURI());
      MDC.put("REQUESTED_METHOD", httpRequest.getMethod());
      LOGGER.info("Request received", httpRequest.getMethod(), httpRequest.getRequestURI());
    }

    try {
      // handle the request
      chain.doFilter(request, response);
    } finally {
    	// cleanup
      MDC.clear();
    }
  }


  /**
   * Destroy life cycle hook. Does nothing for this filter
   */

  public void destroy() {
    // nothing to do
  }

}
