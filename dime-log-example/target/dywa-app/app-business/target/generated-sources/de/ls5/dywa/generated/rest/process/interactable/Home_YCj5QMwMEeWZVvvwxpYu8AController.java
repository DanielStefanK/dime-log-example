package de.ls5.dywa.generated.rest.process.interactable;

import java.util.List;

@javax.transaction.Transactional(dontRollbackOn = info.scce.dime.exception.GUIEncounteredSignal.class)
@javax.ws.rs.Path("/start/Home_YCj5QMwMEeWZVvvwxpYu8A")
public class Home_YCj5QMwMEeWZVvvwxpYu8AController {

	@javax.inject.Inject
	private info.scce.dime.gui.ProcessResumer processResumer;

	@javax.inject.Inject
	private info.scce.dime.rest.ObjectCache objectCache;
	@javax.inject.Inject
	private info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A process;
	
	// controller for fetching dywa entities
	@javax.inject.Inject
	private de.ls5.dywa.generated.util.DomainFileController domainFileController;
	
		@javax.ws.rs.POST
		@javax.ws.rs.Path("public")
		@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
		public javax.ws.rs.core.Response execute(Home_YCj5QMwMEeWZVvvwxpYu8AInput ctx) {
				Home_YCj5QMwMEeWZVvvwxpYu8AOutput result = new Home_YCj5QMwMEeWZVvvwxpYu8AOutput(this.process.execute(false),objectCache);
				return javax.ws.rs.core.Response.ok(result).build();
		}
		
	
	@javax.ws.rs.POST
	@javax.ws.rs.Path("_c_XIICy8Eea_jNcpuXWRlw/public")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response continueAfterLogin_c_XIICy8Eea_jNcpuXWRlw(info.scce.dime.rest.RESTContext ctx) {
		final Object result = this.processResumer.resumeFromGUI(ctx.getCallStack(),null);
		return javax.ws.rs.core.Response.ok(result).build();
	}
	@javax.ws.rs.POST
	@javax.ws.rs.Path("_ee1W0Cy8Eea_jNcpuXWRlw/public")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response continueAfterLogin_ee1W0Cy8Eea_jNcpuXWRlw(info.scce.dime.rest.RESTContext ctx) {
		final Object result = this.processResumer.resumeFromGUI(ctx.getCallStack(),null);
		return javax.ws.rs.core.Response.ok(result).build();
	}
}
