package de.ls5.dywa.generated.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
public interface TODOList extends info.scce.dime.rest.RESTBaseType  {

	java.util.List<
	TODOEntry
	>
	 getentries_TODOEntry();
	boolean isentries_TODOEntrySet();

	void setentries_TODOEntry(final java.util.List<
	TODOEntry
	>
	 entries);
	java.util.List<
	ConcreteUser
	>
	 getowners_ConcreteUser();
	boolean isowners_ConcreteUserSet();

	void setowners_ConcreteUser(final java.util.List<
	ConcreteUser
	>
	 owners);
	java.lang.String
	 getdescription();
	boolean isdescriptionSet();

	void setdescription(final java.lang.String
	 description);

	public static TODOList fromDywaEntity(final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList entity, info.scce.dime.rest.ObjectCache objectCache) {
	
		final TODOList result;
	
		if (entity instanceof de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList) {
			result = new de.ls5.dywa.generated.rest.types.TODOListImpl();
		}
	 else { throw new java.lang.IllegalArgumentException("Unknown type"); } 
		objectCache.putRestTo(entity, result);
	
		return result;
	}
}
