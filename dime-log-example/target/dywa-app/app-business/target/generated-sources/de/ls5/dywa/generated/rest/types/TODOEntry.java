package de.ls5.dywa.generated.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
public interface TODOEntry extends info.scce.dime.rest.RESTBaseType  {

	TODOList
	 getlist();
	boolean islistSet();

	void setlist(final TODOList
	 list);
	java.lang.String
	 getdescription();
	boolean isdescriptionSet();

	void setdescription(final java.lang.String
	 description);

	public static TODOEntry fromDywaEntity(final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry entity, info.scce.dime.rest.ObjectCache objectCache) {
	
		final TODOEntry result;
	
		if (entity instanceof de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry) {
			result = new de.ls5.dywa.generated.rest.types.TODOEntryImpl();
		}
	 else { throw new java.lang.IllegalArgumentException("Unknown type"); } 
		objectCache.putRestTo(entity, result);
	
		return result;
	}
}
