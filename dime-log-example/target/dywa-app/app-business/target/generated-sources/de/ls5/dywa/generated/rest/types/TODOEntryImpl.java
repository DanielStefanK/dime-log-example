package de.ls5.dywa.generated.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
public class TODOEntryImpl extends info.scce.dime.rest.RESTBaseImpl implements TODOEntry
 {

	private TODOList
	 list;
	private boolean islistSet;
	
	@com.fasterxml.jackson.annotation.JsonProperty("list")
	@info.scce.dime.rest.JsonRenderIndicator("islistSet")
	@java.lang.Override
	public TODOList
	 getlist() {
		return this.list;
	}
	
	@com.fasterxml.jackson.annotation.JsonIgnore
	public boolean islistSet() {
		return this.islistSet;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("list")
	@java.lang.Override
	public void setlist(final TODOList
	 list) {
		this.list = list;
		this.islistSet = true;
	}

	private java.lang.String
	 description;
	private boolean isdescriptionSet;
	
	@com.fasterxml.jackson.annotation.JsonProperty("description")
	@info.scce.dime.rest.JsonRenderIndicator("isdescriptionSet")
	@java.lang.Override
	public java.lang.String
	 getdescription() {
		return this.description;
	}
	
	@com.fasterxml.jackson.annotation.JsonIgnore
	public boolean isdescriptionSet() {
		return this.isdescriptionSet;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("description")
	@java.lang.Override
	public void setdescription(final java.lang.String
	 description) {
		this.description = description;
		this.isdescriptionSet = true;
	}

	
}
