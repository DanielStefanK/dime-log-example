// generated by SelectiveGenerator#generateFullSelective(ComplexTypeView view)
package de.ls5.dywa.generated.rest.types;

public class ConcreteUserSelective {

	public static void copy(
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser from,
		final de.ls5.dywa.generated.rest.types.ConcreteUser to,
		final info.scce.dime.rest.ObjectCache objectCache) {


		if (objectCache.containsSelective(to, "ConcreteUserSelective")) {
			return;
		}

		to.setDywaId(from.getDywaId());

		// put to cache, after id has been set
		objectCache.putSelective(to, "ConcreteUserSelective");

		// non-transient objects can be fetched completely from the DB
		if (from.getDywaId() > info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
			return;
		}

		to.setDywaVersion(from.getDywaVersion());
		to.setDywaName(from.getDywaName());

		{
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser effectiveFrom = from;
			final de.ls5.dywa.generated.rest.types.ConcreteUser effectiveTo = to;
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.BaseUser
		 source = effectiveFrom.getbaseUser();
		if (source != null) {
			// original selective: BaseUserSelective
			de.ls5.dywa.generated.rest.types.BaseUser cached = objectCache.getRestTo(source);
			
			if (cached == null) {
				cached = de.ls5.dywa.generated.rest.types.BaseUser.fromDywaEntity(source, objectCache);
			}
			
			if (!objectCache.containsSelective(cached, "BaseUserSelective")) {
				de.ls5.dywa.generated.rest.types.BaseUserSelective.copy(source, cached, objectCache);
			}
		
			effectiveTo.setbaseUser(cached);
		}
		}
		{
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser effectiveFrom = from;
			final de.ls5.dywa.generated.rest.types.ConcreteUser effectiveTo = to;
		final java.util.List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList>
		 source = effectiveFrom.gettodoLists_TODOList();
		final java.util.List<de.ls5.dywa.generated.rest.types.TODOList>
		 target = new java.util.ArrayList<>(source.size());
		
		for (final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
		 s : source) {
			// original selective: TODOListSelective
			de.ls5.dywa.generated.rest.types.TODOList cached = objectCache.getRestTo(s);
			
			if (cached == null) {
				cached = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(s, objectCache);
			}
			
			if (!objectCache.containsSelective(cached, "TODOListSelective")) {
				de.ls5.dywa.generated.rest.types.TODOListSelective.copy(s, cached, objectCache);
			}
		
			target.add(cached);
		}
		
		effectiveTo.settodoLists_TODOList(target);
		}
		{
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser effectiveFrom = from;
			final de.ls5.dywa.generated.rest.types.ConcreteUser effectiveTo = to;
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
		 source = effectiveFrom.getdywaSwitchedTo();
		if (source != null) {
			// original selective: ConcreteUserSelective
			de.ls5.dywa.generated.rest.types.ConcreteUser cached = objectCache.getRestTo(source);
			
			if (cached == null) {
				cached = de.ls5.dywa.generated.rest.types.ConcreteUser.fromDywaEntity(source, objectCache);
			}
			
			if (!objectCache.containsSelective(cached, "ConcreteUserSelective")) {
				de.ls5.dywa.generated.rest.types.ConcreteUserSelective.copy(source, cached, objectCache);
			}
		
			effectiveTo.setdywaSwitchedTo(cached);
		}
		}
	}
}
