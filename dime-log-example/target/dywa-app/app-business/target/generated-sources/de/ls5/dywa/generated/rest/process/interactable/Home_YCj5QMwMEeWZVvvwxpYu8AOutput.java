// generated by info.scce.dime.generator.rest.SelectiveInteractableProcessGenerator#generateOutputs(Process)

package de.ls5.dywa.generated.rest.process.interactable;

import java.util.List;

public class Home_YCj5QMwMEeWZVvvwxpYu8AOutput{
	private String branchName;
	private String branchId;
	private Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper result;
		
	@com.fasterxml.jackson.annotation.JsonProperty("branchName")
	public java.lang.String getBranchName() {
		return this.branchName;
	}
	
	public void setBranchName(String branchname) {
		this.branchName = branchname;
	}
	
	@com.fasterxml.jackson.annotation.JsonProperty("branchId")
	public java.lang.String getBranchId() {
		return this.branchId;
	}
	
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	
	@com.fasterxml.jackson.annotation.JsonProperty("outputs")
	@com.codingrodent.jackson.crypto.Encrypt
	@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
	@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
	public Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper getResult() {
		return this.result;
	}
	
	public void setResult(Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper result) {
		this.result = result;
	}
	public Home_YCj5QMwMEeWZVvvwxpYu8AOutput(){}
	public Home_YCj5QMwMEeWZVvvwxpYu8AOutput(info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A.Home_YCj5QMwMEeWZVvvwxpYu8AResult result, info.scce.dime.rest.ObjectCache objectCache){
		this.branchName = result.getBranchName();
		this.branchId = result.getBranchId();
		this.result = new Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper(result, objectCache);
	}

public static class Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper {
	
	private String branchName;
		
	@com.fasterxml.jackson.annotation.JsonProperty("branchName")
	public java.lang.String getBranchName() {
		return this.branchName;
	}
	
	public void setBranchName(String branchname) {
		this.branchName = branchname;
	}
	public Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper(){}
	public Home_YCj5QMwMEeWZVvvwxpYu8AOutputWrapper(info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A.Home_YCj5QMwMEeWZVvvwxpYu8AResult result, info.scce.dime.rest.ObjectCache objectCache){
		this.branchName = result.getBranchName();
	}
}
}
