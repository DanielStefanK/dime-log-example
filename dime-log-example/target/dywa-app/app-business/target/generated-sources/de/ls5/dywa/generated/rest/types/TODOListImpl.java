package de.ls5.dywa.generated.rest.types;

@com.fasterxml.jackson.annotation.JsonFilter("DIME_Selective_Filter")
@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.voodoodyne.jackson.jsog.JSOGGenerator.class)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, property = info.scce.dime.util.Constants.DYWA_RUNTIME_TYPE)
public class TODOListImpl extends info.scce.dime.rest.RESTBaseImpl implements TODOList
 {

	private java.util.List<
	TODOEntry
	>
	 entries = new java.util.LinkedList<>();
	private boolean isentriesSet;
	
	@com.fasterxml.jackson.annotation.JsonProperty("entries")
	@info.scce.dime.rest.JsonRenderIndicator("isentries_TODOEntrySet")
	@java.lang.Override
	public java.util.List<
	TODOEntry
	>
	 getentries_TODOEntry() {
		return this.entries;
	}
	
	@com.fasterxml.jackson.annotation.JsonIgnore
	public boolean isentries_TODOEntrySet() {
		return this.isentriesSet;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("entries")
	@java.lang.Override
	public void setentries_TODOEntry(final java.util.List<
	TODOEntry
	>
	 entries) {
		this.entries = entries;
		this.isentriesSet = true;
	}

	private java.util.List<
	ConcreteUser
	>
	 owners = new java.util.LinkedList<>();
	private boolean isownersSet;
	
	@com.fasterxml.jackson.annotation.JsonProperty("owners")
	@info.scce.dime.rest.JsonRenderIndicator("isowners_ConcreteUserSet")
	@java.lang.Override
	public java.util.List<
	ConcreteUser
	>
	 getowners_ConcreteUser() {
		return this.owners;
	}
	
	@com.fasterxml.jackson.annotation.JsonIgnore
	public boolean isowners_ConcreteUserSet() {
		return this.isownersSet;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("owners")
	@java.lang.Override
	public void setowners_ConcreteUser(final java.util.List<
	ConcreteUser
	>
	 owners) {
		this.owners = owners;
		this.isownersSet = true;
	}

	private java.lang.String
	 description;
	private boolean isdescriptionSet;
	
	@com.fasterxml.jackson.annotation.JsonProperty("description")
	@info.scce.dime.rest.JsonRenderIndicator("isdescriptionSet")
	@java.lang.Override
	public java.lang.String
	 getdescription() {
		return this.description;
	}
	
	@com.fasterxml.jackson.annotation.JsonIgnore
	public boolean isdescriptionSet() {
		return this.isdescriptionSet;
	}

	@com.fasterxml.jackson.annotation.JsonProperty("description")
	@java.lang.Override
	public void setdescription(final java.lang.String
	 description) {
		this.description = description;
		this.isdescriptionSet = true;
	}

	
}
