// Generator: info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)

package info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home;

public class Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult {
	
	private String branchName;
	
	public String getBranchName() {
		return this.branchName;
	}
	
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName) {
		this.branchName = branchName;
	}
	
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn) {
		this.branchName = branchName;
		this.gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn = gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	}
	
	private gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	
	public gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn getgui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn() {
		return gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	}
	
	public static class gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser CurrentOwner;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentOwner")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser getCurrentOwner() {
			return CurrentOwner;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentOwner")
		public void setCurrentOwner(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser CurrentOwner) {
			this.CurrentOwner = CurrentOwner;
		}
	}
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn) {
		this.branchName = branchName;
		this.gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn = gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	}
	
	private gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	
	public gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn getgui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn() {
		return gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	}
	
	public static class gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry NewEntry;
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewEntry")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry getNewEntry() {
			return NewEntry;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewEntry")
		public void setNewEntry(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry NewEntry) {
			this.NewEntry = NewEntry;
		}
	}
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn) {
		this.branchName = branchName;
		this.gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn = gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	private gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	
	public gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn() {
		return gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	public static class gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser;
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser getChosenUser() {
			return ChosenUser;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public void setChosenUser(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser) {
			this.ChosenUser = ChosenUser;
		}
	}
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn) {
		this.branchName = branchName;
		this.gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn = gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	}
	
	private gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	
	public gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn getgui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn() {
		return gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	}
	
	public static class gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry CurrentEntry;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentEntry")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry getCurrentEntry() {
			return CurrentEntry;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentEntry")
		public void setCurrentEntry(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry CurrentEntry) {
			this.CurrentEntry = CurrentEntry;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
	}
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn) {
		this.branchName = branchName;
		this.gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn = gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn;
	}
	
	private gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn;
	
	public gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn getgui__CqcGAPvsEeWWodZOLky4iQRemoveReturn() {
		return gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn;
	}
	
	public static class gui__CqcGAPvsEeWWodZOLky4iQRemoveReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
	}
	
	public Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult(String branchName, gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn) {
		this.branchName = branchName;
		this.gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn = gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	}
	
	private gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	
	public gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn getgui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn() {
		return gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	}
	
	public static class gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList NewList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getNewList() {
			return NewList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewList")
		public void setNewList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList NewList) {
			this.NewList = NewList;
		}
	}
}
