package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public final class RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYg implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// context variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist;
		// direct dataflow variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser startOwnerToRemove_ROBecNMfEeWJV9QOcxWQYg;

		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;

	@Inject
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYg(final BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	private Context createContext(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList list, de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ownerToRemove) {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		ctx.todolist = list;
		ctx.startOwnerToRemove_ROBecNMfEeWJV9QOcxWQYg = ownerToRemove;
		
		return ctx;
	}

	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult execute(boolean isAuthenticationRequired,de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList list, de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ownerToRemove) {
		final Context ctx = createContext(list, ownerToRemove);
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult executeInternal(final Context ctx) {
		return execute_MxJckPzPEeaVBcOQNPoz0Q(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private RemovedReturn removed;
		
		public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult(RemovedReturn removed) {
			this.branchName = "removed";
			this.branchId = "_POR0QNMfEeWJV9QOcxWQYg";
			this.removed = removed;
		}
		private Not_removedReturn not_removed;
		
		public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult(Not_removedReturn not_removed) {
			this.branchName = "not removed";
			this.branchId = "_XvtEYN70EeWlD7knc3NOdQ";
			this.not_removed = not_removed;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public RemovedReturn getRemovedReturn() {
			return removed;
		}
		public Not_removedReturn getNot_removedReturn() {
			return not_removed;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>removed</code>.
		 */
		public interface RemovedReturn {
		}
	
		/**
		 * Return type of branch <code>removed</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class RemovedReturnImpl implements RemovedReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(RemovedReturnImpl.class);

			private final Context ctx;


	        RemovedReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		
		/**
		 * Interface definition for return type of branch <code>not removed</code>.
		 */
		public interface Not_removedReturn {
		}
	
		/**
		 * Return type of branch <code>not removed</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class Not_removedReturnImpl implements Not_removedReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(Not_removedReturnImpl.class);

			private final Context ctx;


	        Not_removedReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	
	

	// sibs
	// container for graph i/o 'removed'.
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult execute_POR0QNMfEeWJV9QOcxWQYg(final Context ctx) {
		return new RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult(new RemovedReturnImpl(ctx));
	}
	// container for remove from list SIB 'RemoveFromList'.
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult execute_Vk8bYNMfEeWJV9QOcxWQYg(final Context ctx) {
		if (Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getowners_ConcreteUser).orElse(new LinkedList<>()).remove(ctx.startOwnerToRemove_ROBecNMfEeWJV9QOcxWQYg)) {
			// branch 'removed'
			return execute_POR0QNMfEeWJV9QOcxWQYg(ctx);
		}
		// branch 'not found'
		throw new IllegalStateException("SIB 'RemoveFromList' has no successor defined for branch 'not found'");
	}
	// container for graph i/o 'not removed'.
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult execute_XvtEYN70EeWlD7knc3NOdQ(final Context ctx) {
		return new RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult(new Not_removedReturnImpl(ctx));
	}
	// container for atomic SIB 'IntegerEquals'.
	public RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult execute_MxJckPzPEeaVBcOQNPoz0Q(final Context ctx) {
		if (info.scce.dime.common.CommonNativeServiceLibrary.integerEquals(Long.valueOf(Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getowners_ConcreteUser).map(List::size).orElse(0)), 1l)) {
			// branch 'true'
			return execute_XvtEYN70EeWlD7knc3NOdQ(ctx);
		}
		else {
			// branch 'false'
			return execute_Vk8bYNMfEeWJV9QOcxWQYg(ctx);
		}
	}
	
}  
