package info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public final class SubInteraction_nMpSEA3cEeaEdIuIovP4yg implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// direct dataflow variables.
		private List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> GetAllUsersSUccessAllUsers_e__HYPHEN_MINUS__w__HYPHEN_MINUS__wA3pEea41K__HYPHEN_MINUS__gq5fyQA = new ArrayList<>();

		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;

	@Inject
	public SubInteraction_nMpSEA3cEeaEdIuIovP4yg(final BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	private Context createContext() {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		
		return ctx;
	}

	public SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult execute(boolean isAuthenticationRequired) {
		final Context ctx = createContext();
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult executeInternal(final Context ctx) {
		return execute_e9WCcA3pEea41K__HYPHEN_MINUS__gq5fyQA(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private DelfinReturn delfin;
		
		public SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult(DelfinReturn delfin) {
			this.branchName = "delfin";
			this.branchId = "_nP6ogA3cEeaEdIuIovP4yg";
			this.delfin = delfin;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public DelfinReturn getDelfinReturn() {
			return delfin;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>delfin</code>.
		 */
		public interface DelfinReturn {
			public List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> getConcreteuser1();
		}
	
		/**
		 * Return type of branch <code>delfin</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class DelfinReturnImpl implements DelfinReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(DelfinReturnImpl.class);

			private final Context ctx;


	        DelfinReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

			public List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> getConcreteuser1() {
				return ctx.GetAllUsersSUccessAllUsers_e__HYPHEN_MINUS__w__HYPHEN_MINUS__wA3pEea41K__HYPHEN_MINUS__gq5fyQA;
			}
		}
		

	
	

	// sibs
	// container for graph abstraction 'GetAllUsers' and sub process 'GetAllUsers'.
	public SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult execute_e9WCcA3pEea41K__HYPHEN_MINUS__gq5fyQA(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.GetAllUsers_l6zBwNMaEeWJV9QOcxWQYg instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.GetAllUsers_l6zBwNMaEeWJV9QOcxWQYg.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.GetAllUsers_l6zBwNMaEeWJV9QOcxWQYg.GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult result = instance.execute(false);
	
		switch(result.getBranchName()) {
		case "SUccess": {
			List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> value6 = result.getSUccessReturn().getAllUsers();
			if (value6 != null) { // prevent null in lists
			ctx.GetAllUsersSUccessAllUsers_e__HYPHEN_MINUS__w__HYPHEN_MINUS__wA3pEea41K__HYPHEN_MINUS__gq5fyQA = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(value6);
			}
			// branch 'SUccess'
			return execute_nP6ogA3cEeaEdIuIovP4yg(ctx);
		}
			default: throw new IllegalStateException("SIB 'GetAllUsers' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph i/o 'delfin'.
	public SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult execute_nP6ogA3cEeaEdIuIovP4yg(final Context ctx) {
		return new SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult(new DelfinReturnImpl(ctx));
	}
	
}  
