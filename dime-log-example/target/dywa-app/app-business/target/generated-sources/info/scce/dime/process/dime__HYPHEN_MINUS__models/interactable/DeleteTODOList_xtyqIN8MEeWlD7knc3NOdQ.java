package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@info.scce.dime.process.RequiresDI
public final class DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQ implements DIMEProcess {

			// helper context for JSON serialization.
			@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
			public static class JSONContext extends info.scce.dime.process.JSONContext {

				// begin context variables
				// end context variables

				// begin direct dataflow variables
				// end direct dataflow variables

				// begin index variables of iterate sibs
				private int counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = 0;
				// endindex variables of iterate sibs

				public static JSONContext toJSON(BeanManager bm, Context ctx) {
					final JSONContext result = new JSONContext();
					final info.scce.dime.rest.ObjectCache objectCache = new info.scce.dime.rest.ObjectCache();

					result.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = ctx.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w;

					return result;
				}

				@Override
		        public Context toContext(BeanManager bm, ProcessCallFrame callStack) {
					final Context result = new Context();
		            result.beanManager = bm;
					result.callStack = callStack;



					result.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = this.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w;

					return result;
		        }
			}

	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// context variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist;
		// direct dataflow variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry Iterate_TODOEntrysNextElement_IeDQQN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w;
		// index variables of iterate sibs.
		private int counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = 0;

		public info.scce.dime.process.JSONContext toJSON() {
			return JSONContext.toJSON(beanManager, this);
        }
	}

	private final BeanManager beanManager;
	private final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOEntryController TODOEntryController;
	private final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOListController TODOListController;

	@Inject
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQ(final BeanManager beanManager, de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOEntryController TODOEntryController, de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOListController TODOListController) {
		this.beanManager = beanManager;
		this.TODOEntryController = TODOEntryController;
		this.TODOListController = TODOListController;
	}

	private Context createContext(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todoList) {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		ctx.todolist = todoList;
		
		return ctx;
	}

	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult execute(boolean isAuthenticationRequired,de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todoList) {
		final Context ctx = createContext(todoList);
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		final Context ctx = ((JSONContext) context).toContext(this.beanManager, callStack);

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult executeInternal(final Context ctx) {
		return execute_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private DeletedReturn deleted;
		
		public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult(DeletedReturn deleted) {
			this.branchName = "deleted";
			this.branchId = "_xudYgN8MEeWlD7knc3NOdQ";
			this.deleted = deleted;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public DeletedReturn getDeletedReturn() {
			return deleted;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>deleted</code>.
		 */
		public interface DeletedReturn {
		}
	
		/**
		 * Return type of branch <code>deleted</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class DeletedReturnImpl implements DeletedReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(DeletedReturnImpl.class);

			private final Context ctx;


	        DeletedReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	
	

	// sibs
	// container for iterate complex SIB 'Iterate TODOEntrys'.
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult execute_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w(final Context ctx) {
		final int pos = ctx.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w++;
		List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry> list = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry>(Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getentries_TODOEntry).orElse(new LinkedList<>()));
		if(list != null && pos < list.size()) {
			ctx.Iterate_TODOEntrysNextElement_IeDQQN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry>(Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getentries_TODOEntry).orElse(new LinkedList<>())).get(pos);
			// branch 'next'
			return execute_LZXK0N__HYPHEN_MINUS__ZEeWsv_pvkbCq6w(ctx);
		}
		else {
			ctx.counterIterate_TODOEntrys_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w = 0;
			// branch 'exit'
			return execute_8KujEN8MEeWlD7knc3NOdQ(ctx);
		}
		
	}
	// container for delete SIB 'Delete'.
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult execute_LZXK0N__HYPHEN_MINUS__ZEeWsv_pvkbCq6w(final Context ctx) {
		{
			final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOEntryController domController = this.TODOEntryController;
	
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry instanceToDelete = ctx.Iterate_TODOEntrysNextElement_IeDQQN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w;
			if (instanceToDelete != null) {
			
				// Clear attribute values in order to release all bidirectional associations from this object.
				instanceToDelete.setlist(null);
				domController.deleteWithIncomingReferences(instanceToDelete);
			}
		}
		// branch 'deleted'
		return execute_IdRNIN__HYPHEN_MINUS__ZEeWsv_pvkbCq6w(ctx);
	}
	// container for graph i/o 'deleted'.
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult execute_xudYgN8MEeWlD7knc3NOdQ(final Context ctx) {
		return new DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult(new DeletedReturnImpl(ctx));
	}
	// container for delete SIB 'Delete'.
	public DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult execute_8KujEN8MEeWlD7knc3NOdQ(final Context ctx) {
		{
			final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOListController domController = this.TODOListController;
	
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList instanceToDelete = ctx.todolist;
			if (instanceToDelete != null) {
			
				// Clear attribute values in order to release all bidirectional associations from this object.
				instanceToDelete.getentries_TODOEntry().clear();
				instanceToDelete.getowners_ConcreteUser().clear();
				domController.deleteWithIncomingReferences(instanceToDelete);
			}
		}
		// branch 'deleted'
		return execute_xudYgN8MEeWlD7knc3NOdQ(ctx);
	}
	
}  
