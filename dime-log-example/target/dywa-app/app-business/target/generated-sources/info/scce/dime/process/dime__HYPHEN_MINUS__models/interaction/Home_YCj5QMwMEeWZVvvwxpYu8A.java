package info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public final class Home_YCj5QMwMEeWZVvvwxpYu8A implements DIMEProcess {

			// helper context for JSON serialization.
			@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
			public static class JSONContext extends info.scce.dime.process.JSONContext {

				// begin context variables
				// end context variables

				// begin direct dataflow variables
				private List<de.ls5.dywa.generated.rest.types.ConcreteUser> guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = new ArrayList<>();
				private de.ls5.dywa.generated.rest.types.TODOList HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOEntry HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOList HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.ConcreteUser HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.ConcreteUser HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOList HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOList HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOEntry HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOList HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg;
				private de.ls5.dywa.generated.rest.types.TODOList HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ;
				// end direct dataflow variables

				// begin index variables of iterate sibs
				// endindex variables of iterate sibs

				public static JSONContext toJSON(BeanManager bm, Context ctx) {
					final JSONContext result = new JSONContext();
					final info.scce.dime.rest.ObjectCache objectCache = new info.scce.dime.rest.ObjectCache();

					result.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = new java.util.ArrayList<>(ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw.size());
								
					for (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser o : ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw.stream().filter((n)->n!=null).collect(java.util.stream.Collectors.toList())) {
						final de.ls5.dywa.generated.rest.types.ConcreteUser trans = de.ls5.dywa.generated.rest.types.ConcreteUser.fromDywaEntity(o, objectCache);
						de.ls5.dywa.generated.rest.types.ConcreteUserSelective.copy(o, trans, objectCache);
						result.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw.add(trans);
					}
					if(ctx.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg != null) {
						result.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg, result.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg != null) {
						result.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOEntry.fromDywaEntity(ctx.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOEntrySelective.copy(ctx.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg, result.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg != null) {
						result.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg, result.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg != null) {
						result.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.ConcreteUser.fromDywaEntity(ctx.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.ConcreteUserSelective.copy(ctx.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg, result.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg != null) {
						result.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.ConcreteUser.fromDywaEntity(ctx.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.ConcreteUserSelective.copy(ctx.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg, result.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg != null) {
						result.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg, result.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg != null) {
						result.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg, result.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg != null) {
						result.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOEntry.fromDywaEntity(ctx.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOEntrySelective.copy(ctx.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg, result.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg != null) {
						result.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg, result.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg, objectCache);
					}
					if(ctx.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ != null) {
						result.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ = de.ls5.dywa.generated.rest.types.TODOList.fromDywaEntity(ctx.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ, objectCache);
						de.ls5.dywa.generated.rest.types.TODOListSelective.copy(ctx.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ, result.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ, objectCache);
					}

					return result;
				}

				@Override
		        public Context toContext(BeanManager bm, ProcessCallFrame callStack) {
					final Context result = new Context();
		            result.beanManager = bm;
					result.callStack = callStack;


					info.scce.dime.process.ContextTransformer contextTransformer = info.scce.dime.process.ContextTransformer.getInstance(bm);

					result.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = this.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw.stream().filter(o -> o != null).map(o -> contextTransformer.transform(o)).collect(Collectors.toList());
					if (this.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg != null) {
						result.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg);
					}
					if (this.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg != null) {
						result.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg);
					}
					if (this.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg != null) {
						result.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg);
					}
					if (this.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg != null) {
						result.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg);
					}
					if (this.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg != null) {
						result.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg);
					}
					if (this.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg != null) {
						result.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg);
					}
					if (this.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg != null) {
						result.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg);
					}
					if (this.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg != null) {
						result.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg);
					}
					if (this.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg != null) {
						result.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg = contextTransformer.transform(this.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg);
					}
					if (this.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ != null) {
						result.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ = contextTransformer.transform(this.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ);
					}

					return result;
		        }
			}

	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// direct dataflow variables.
		private List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = new ArrayList<>();
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg;
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ;

		public info.scce.dime.process.JSONContext toJSON() {
			return JSONContext.toJSON(beanManager, this);
        }
	}

	private final BeanManager beanManager;

	@Inject
	public Home_YCj5QMwMEeWZVvvwxpYu8A(final BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	private Context createContext() {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		
		return ctx;
	}

	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute(boolean isAuthenticationRequired) {
		final Context ctx = createContext();
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute(ProcessCallFrame callStack) {
		final Context ctx = createContext();
		ctx.callStack = callStack;

		return executeInternal(ctx);
	}

	@Override
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		final Context ctx = ((JSONContext) context).toContext(this.beanManager, callStack);

		switch (sibId) {
		case "_zEoYcMwMEeWZVvvwxpYu8A": return continue_zEoYcMwMEeWZVvvwxpYu8A(ctx, slgResult);
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private Home_YCj5QMwMEeWZVvvwxpYu8AResult executeInternal(final Context ctx) {
		return execute_c_XIICy8Eea_jNcpuXWRlw(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class Home_YCj5QMwMEeWZVvvwxpYu8AResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		

	}
	
	// model branches.

	
	

	// sibs
	// Input class -- generated by info.scce.dime.generator.process.BackendProcessGeneratorHelper#renderInputClass(SIB)
	//   for SIB Home _zEoYcMwMEeWZVvvwxpYu8A
	private static class InputsForGUISIB_zEoYcMwMEeWZVvvwxpYu8A {
		public java.util.List<de.ls5.dywa.generated.rest.types.ConcreteUser>
		 allUsers = new ArrayList<>();
	}
	
	
	// container for GUI SIB 'Home'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_zEoYcMwMEeWZVvvwxpYu8A(final Context ctx) {
	
		final info.scce.dime.rest.ObjectCache objectCache = info.scce.dime.util.CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.rest.ObjectCache.class);
		final de.ls5.dywa.generated.rest.controller.ConcreteUserREST inputsConcreteUserREST = info.scce.dime.util.CDIUtil.getManagedInstance(ctx.beanManager, de.ls5.dywa.generated.rest.controller.ConcreteUserREST.class);
		
		final InputsForGUISIB_zEoYcMwMEeWZVvvwxpYu8A inputs = new InputsForGUISIB_zEoYcMwMEeWZVvvwxpYu8A();
		
		{
			//allUsers
			java.util.List<de.ls5.dywa.generated.rest.types.ConcreteUser>
			 result = new java.util.ArrayList<>(ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw.size());
			for (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser obj : ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw) {
			
			de.ls5.dywa.generated.rest.types.ConcreteUser restTO;
			
			if (obj != null) {
				if (obj.getDywaId() > info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
					// read_HomeConcreteUserSelectivex2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A
					restTO = inputsConcreteUserREST.read_HomeConcreteUserSelectivex2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A(obj.getDywaId());
				}
				else {
					restTO = objectCache.getRestTo(obj);
					if (restTO == null) {
						restTO = de.ls5.dywa.generated.rest.types.ConcreteUser.fromDywaEntity(obj, objectCache);
					}
					// HomeConcreteUserSelectivex2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A
					de.ls5.dywa.generated.rest.types.HomeConcreteUserSelectivex2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A.copy(obj, restTO, objectCache);
				}
				result.add(restTO);
			}
			}
			inputs.allUsers = result;
		}
		
		final CallFrame currentFrame = new CallFrame("info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A:_zEoYcMwMEeWZVvvwxpYu8A:_gF-WEMwMEeWZVvvwxpYu8A", ctx, ctx.lastMajorSIBId);
		
		ctx.callStack.getCallFrames().add(currentFrame);
	
		GUIEncounteredSignal signal = new GUIEncounteredSignal(ctx.callStack, "_zEoYcMwMEeWZVvvwxpYu8A", inputs);
		
		throw signal;
	}
	
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult continue_zEoYcMwMEeWZVvvwxpYu8A(Context ctx, Object guiResult) {
		if(ctx.callStack.isAuthenticationRequired()) {
			final Subject shiroSubj = SecurityUtils.getSubject();
			if (!shiroSubj.isAuthenticated()) {
				GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "_zEoYcMwMEeWZVvvwxpYu8A");
				sig.setStatus(401);
				throw sig;
			}
		}
		
		ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size()-1);
		
		final info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult result = (info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.Home_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8AResult) guiResult;
		if ("Add_TODO".equals(result.getBranchName())) {
			if(result.getgui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn() != null) {
			ctx.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn().getCurrentList();
			ctx.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry)result.getgui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn().getNewEntry();
			}
			// branch 'Add TODO'
			return execute_MKsO0NMcEeWJV9QOcxWQYg(ctx);
		} else 
		if ("Add_Owner".equals(result.getBranchName())) {
			if(result.getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn() != null) {
			ctx.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser)result.getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn().getChosenUser();
			ctx.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn().getCurrentList();
			}
			// branch 'Add Owner'
			return execute_y9gtUNMeEeWJV9QOcxWQYg(ctx);
		} else 
		if ("Delete_TODO".equals(result.getBranchName())) {
			if(result.getgui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn() != null) {
			ctx.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry)result.getgui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn().getCurrentEntry();
			ctx.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn().getCurrentList();
			}
			// branch 'Delete TODO'
			return execute_D4HpwNMeEeWJV9QOcxWQYg(ctx);
		} else 
		if ("Remove_Owner".equals(result.getBranchName())) {
			if(result.getgui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn() != null) {
			ctx.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser)result.getgui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn().getCurrentOwner();
			ctx.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn().getCurrentList();
			}
			// branch 'Remove Owner'
			return execute_liZ9oNMfEeWJV9QOcxWQYg(ctx);
		} else 
		if ("Add_List".equals(result.getBranchName())) {
			if(result.getgui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn() != null) {
			ctx.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn().getNewList();
			}
			// branch 'Add List'
			return execute_gcuIINMdEeWJV9QOcxWQYg(ctx);
		} else 
		if ("Remove".equals(result.getBranchName())) {
			if(result.getgui__CqcGAPvsEeWWodZOLky4iQRemoveReturn() != null) {
			ctx.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList)result.getgui__CqcGAPvsEeWWodZOLky4iQRemoveReturn().getCurrentList();
			}
			// branch 'Remove'
			return execute_ee1W0Cy8Eea_jNcpuXWRlw(ctx);
		}
		else {
			// unspecified branch, show same GUI again
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
	}
	// container for graph abstraction 'DeleteTODO' and sub process 'DeleteTODO'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_D4HpwNMeEeWJV9QOcxWQYg(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODO_wCKzANMdEeWJV9QOcxWQYg instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODO_wCKzANMdEeWJV9QOcxWQYg(ctx.beanManager);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODO_wCKzANMdEeWJV9QOcxWQYg.DeleteTODO_wCKzANMdEeWJV9QOcxWQYgResult result = instance.execute(false, ctx.HomeDelete_TODOCurrentEntry_jj__HYPHEN_MINUS__pcNMcEeWJV9QOcxWQYg, ctx.HomeDelete_TODOCurrentList_jkC64NMcEeWJV9QOcxWQYg);
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'DeleteTODO' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph abstraction 'RemoveTODOListOwner' and sub process 'RemoveTODOListOwner'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_liZ9oNMfEeWJV9QOcxWQYg(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYg instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYg(ctx.beanManager);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYg.RemoveTODOListOwner_POLGkNMfEeWJV9QOcxWQYgResult result = instance.execute(false, ctx.HomeRemove_OwnerCurrentList_jkN6ANMcEeWJV9QOcxWQYg, ctx.HomeRemove_OwnerCurrentOwner_jkIacNMcEeWJV9QOcxWQYg);
	
		switch(result.getBranchName()) {
		case "removed": {
			// branch 'removed'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
		case "not removed": {
			// branch 'not removed'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'RemoveTODOListOwner' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph abstraction 'AddTODOListOwner' and sub process 'AddTODOListOwner'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_y9gtUNMeEeWJV9QOcxWQYg(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOListOwner_n_gusNMeEeWJV9QOcxWQYg instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOListOwner_n_gusNMeEeWJV9QOcxWQYg(ctx.beanManager);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOListOwner_n_gusNMeEeWJV9QOcxWQYg.AddTODOListOwner_n_gusNMeEeWJV9QOcxWQYgResult result = instance.execute(false, ctx.HomeAdd_OwnerCurrentList_7TF4MNMeEeWJV9QOcxWQYg, ctx.HomeAdd_OwnerChosenUser_an6YQNMaEeWJV9QOcxWQYg);
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'AddTODOListOwner' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph abstraction 'AddTODO' and sub process 'AddTODO'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_MKsO0NMcEeWJV9QOcxWQYg(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODO_6e8l0NMbEeWJV9QOcxWQYg instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODO_6e8l0NMbEeWJV9QOcxWQYg(ctx.beanManager);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODO_6e8l0NMbEeWJV9QOcxWQYg.AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult result = instance.execute(false, ctx.HomeAdd_TODOCurrentList_FT5dgNMZEeWJV9QOcxWQYg, ctx.HomeAdd_TODONewEntry_FUHf8NMZEeWJV9QOcxWQYg);
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'AddTODO' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph abstraction 'AddTODOList' and sub process 'AddTODOList'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_gcuIINMdEeWJV9QOcxWQYg(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOList_JgutgNMdEeWJV9QOcxWQYg instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOList_JgutgNMdEeWJV9QOcxWQYg.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.AddTODOList_JgutgNMdEeWJV9QOcxWQYg.AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult result = instance.execute(false, ctx.HomeAdd_ListNewList_Gxm00NMdEeWJV9QOcxWQYg);
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'AddTODOList' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// guard container for guarded process 'SubInteraction' and sub process 'SubInteraction'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_c_XIICy8Eea_jNcpuXWRlw(final Context ctx) {
		//check authentication
		final Subject shiroSubj = SecurityUtils.getSubject();
		if (!shiroSubj.isAuthenticated()) {
			final CallFrame currentFrame = new CallFrame("info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A:_c_XIICy8Eea_jNcpuXWRlw", ctx);
			ctx.callStack.getCallFrames().add(currentFrame);
			GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "_c_XIICy8Eea_jNcpuXWRlw");
			sig.setStatus(401);
			throw sig;
		}
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubInteraction_nMpSEA3cEeaEdIuIovP4yg instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubInteraction_nMpSEA3cEeaEdIuIovP4yg(ctx.beanManager);
	
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubInteraction_nMpSEA3cEeaEdIuIovP4yg.SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult result = instance.execute(ctx.callStack.isAuthenticationRequired() );
	
		switch (result.getBranchName()) {
		case "delfin": {
			List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> value1 = result.getDelfinReturn().getConcreteuser1();
			if (value1 != null) { // prevent null in lists
			ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(value1);
			}
			// branch 'delfin'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'SubInteraction' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult continue_c_XIICy8Eea_jNcpuXWRlw(Context ctx, Object slgResult) {
		ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size() - 1);
	
		if(slgResult == null) {
			//re-execute the guard container
			return execute_c_XIICy8Eea_jNcpuXWRlw(ctx);
		}
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubInteraction_nMpSEA3cEeaEdIuIovP4yg.SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult result = (info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubInteraction_nMpSEA3cEeaEdIuIovP4yg.SubInteraction_nMpSEA3cEeaEdIuIovP4ygResult) slgResult;
	
		switch (result.getBranchName()) {
		case "delfin": {
			List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> value2 = result.getDelfinReturn().getConcreteuser1();
			if (value2 != null) { // prevent null in lists
			ctx.guarded_SubInteractionDelfinConcreteuser1_ijwxoCmXEeajUcj9Sx0OAw = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(value2);
			}
			// branch 'delfin'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'SubInteraction' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	// guard container for guarded process 'SubDeleteTODOList' and sub process 'SubDeleteTODOList'.
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult execute_ee1W0Cy8Eea_jNcpuXWRlw(final Context ctx) {
		//check authentication
		final Subject shiroSubj = SecurityUtils.getSubject();
		if (!shiroSubj.isAuthenticated()) {
			final CallFrame currentFrame = new CallFrame("info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.Home_YCj5QMwMEeWZVvvwxpYu8A:_ee1W0Cy8Eea_jNcpuXWRlw", ctx);
			ctx.callStack.getCallFrames().add(currentFrame);
			GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "_ee1W0Cy8Eea_jNcpuXWRlw");
			sig.setStatus(401);
			throw sig;
		}
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA instance = new info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA(ctx.beanManager);
	
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult result = instance.execute(ctx.callStack.isAuthenticationRequired() , ctx.HomeRemoveCurrentList_sOBOwN8MEeWlD7knc3NOdQ);
	
		switch (result.getBranchName()) {
		case "deleted": {
			// branch 'deleted'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'SubDeleteTODOList' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	public Home_YCj5QMwMEeWZVvvwxpYu8AResult continue_ee1W0Cy8Eea_jNcpuXWRlw(Context ctx, Object slgResult) {
		ctx.callStack.getCallFrames().remove(ctx.callStack.getCallFrames().size() - 1);
	
		if(slgResult == null) {
			//re-execute the guard container
			return execute_ee1W0Cy8Eea_jNcpuXWRlw(ctx);
		}
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult result = (info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA.SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult) slgResult;
	
		switch (result.getBranchName()) {
		case "deleted": {
			// branch 'deleted'
			return execute_zEoYcMwMEeWZVvvwxpYu8A(ctx);
		}
			default: throw new IllegalStateException("SIB 'SubDeleteTODOList' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
}  
