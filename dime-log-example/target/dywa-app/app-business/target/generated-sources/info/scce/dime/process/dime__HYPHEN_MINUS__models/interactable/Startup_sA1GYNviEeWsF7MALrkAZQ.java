package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@info.scce.dime.process.RequiresDI
public final class Startup_sA1GYNviEeWsF7MALrkAZQ implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();


		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;
	private final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController ConcreteUserController;

	@Inject
	public Startup_sA1GYNviEeWsF7MALrkAZQ(final BeanManager beanManager, de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController ConcreteUserController) {
		this.beanManager = beanManager;
		this.ConcreteUserController = ConcreteUserController;
	}

	private Context createContext() {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		
		return ctx;
	}

	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute(boolean isAuthenticationRequired) {
		final Context ctx = createContext();
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public Startup_sA1GYNviEeWsF7MALrkAZQResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private Startup_sA1GYNviEeWsF7MALrkAZQResult executeInternal(final Context ctx) {
		return execute_PXMhsNzNEeWtAOZsxPLKSA(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class Startup_sA1GYNviEeWsF7MALrkAZQResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private SuccessReturn success;
		
		public Startup_sA1GYNviEeWsF7MALrkAZQResult(SuccessReturn success) {
			this.branchName = "success";
			this.branchId = "_sBOvANviEeWsF7MALrkAZQ";
			this.success = success;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public SuccessReturn getSuccessReturn() {
			return success;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>success</code>.
		 */
		public interface SuccessReturn {
		}
	
		/**
		 * Return type of branch <code>success</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class SuccessReturnImpl implements SuccessReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(SuccessReturnImpl.class);

			private final Context ctx;


	        SuccessReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	
	

	// sibs
	// container for retrieve all of type (matching the given constraints) SIB 'Retrieve all ConcreteUser'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_PXMhsNzNEeWtAOZsxPLKSA(final Context ctx) {
		final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController domController = this.ConcreteUserController;
	
		// search for all objects of type matching the given contraints.
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser searchObject = domController.createSearchObject("");
		final List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> result = domController.findByProperties(searchObject);
		if (result.isEmpty()) {
			// branch 'none found'
			return execute_MPmiYdgDEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
		}
		else {
			// branch 'success'
			return execute_96swQPzPEeaVBcOQNPoz0Q(ctx);
		}
	}
	// container for graph abstraction 'CreateUser' and sub process 'CreateUser'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_G8JPsNzHEeW2rv4DARGjjw(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.CreateUser_cBxicNzDEeW2rv4DARGjjwResult result = instance.execute(false, "Mary Jane", "Watson", "pwd", "mary");
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_ggHOM9gDEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
		}
			default: throw new IllegalStateException("SIB 'CreateUser' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph i/o 'success'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_sBOvANviEeWsF7MALrkAZQ(final Context ctx) {
		return new Startup_sA1GYNviEeWsF7MALrkAZQResult(new SuccessReturnImpl(ctx));
	}
	// container for graph abstraction 'CreateUser' and sub process 'CreateUser'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_iHeOANzGEeW2rv4DARGjjw(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.CreateUser_cBxicNzDEeW2rv4DARGjjwResult result = instance.execute(false, "J. Jonah", "Jameson ", "pwd", "jonah");
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_G8JPsNzHEeW2rv4DARGjjw(ctx);
		}
			default: throw new IllegalStateException("SIB 'CreateUser' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for graph abstraction 'CreateUser' and sub process 'CreateUser'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_PssUwNzGEeW2rv4DARGjjw(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.basic.CreateUser_cBxicNzDEeW2rv4DARGjjw.CreateUser_cBxicNzDEeW2rv4DARGjjwResult result = instance.execute(false, "Peter", "Parker", "pwd", "peter");
	
		switch(result.getBranchName()) {
		case "success": {
			// branch 'success'
			return execute_WUaPMdgDEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
		}
			default: throw new IllegalStateException("SIB 'CreateUser' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	// container for atomic SIB 'LogDebugMessage'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_96swQPzPEeaVBcOQNPoz0Q(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.logDebugMessage("Skipping initialization");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'LogDebugMessage' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_sBOvANviEeWsF7MALrkAZQ(ctx);
	}
	// container for atomic SIB 'LogWarnMessage'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_MPmiYdgDEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.logWarnMessage("no user found. creating inital users.");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'LogWarnMessage' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_PssUwNzGEeW2rv4DARGjjw(ctx);
	}
	// container for atomic SIB 'LogInfoMessage'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_WUaPMdgDEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.logInfoMessage("peter was created");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'LogInfoMessage' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_iHeOANzGEeW2rv4DARGjjw(ctx);
	}
	// container for atomic SIB 'LogInfoMessage'.
	public Startup_sA1GYNviEeWsF7MALrkAZQResult execute_ggHOM9gDEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.logInfoMessage("mary was created");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'LogInfoMessage' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_sBOvANviEeWsF7MALrkAZQ(ctx);
	}
	
}  
