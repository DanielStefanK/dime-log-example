package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@info.scce.dime.process.RequiresDI
public final class AddTODOList_JgutgNMdEeWJV9QOcxWQYg implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// context variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist;

		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;
	private final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.BaseUserController BaseUserController;

	@Inject
	public AddTODOList_JgutgNMdEeWJV9QOcxWQYg(final BeanManager beanManager, de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.BaseUserController BaseUserController) {
		this.beanManager = beanManager;
		this.BaseUserController = BaseUserController;
	}

	private Context createContext(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList newList) {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		ctx.todolist = newList;
		
		return ctx;
	}

	public AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult execute(boolean isAuthenticationRequired,de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList newList) {
		final Context ctx = createContext(newList);
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult executeInternal(final Context ctx) {
		return execute_POloMNMdEeWJV9QOcxWQYg(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private SuccessReturn success;
		
		public AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult(SuccessReturn success) {
			this.branchName = "success";
			this.branchId = "_Jg3QYNMdEeWJV9QOcxWQYg";
			this.success = success;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public SuccessReturn getSuccessReturn() {
			return success;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>success</code>.
		 */
		public interface SuccessReturn {
		}
	
		/**
		 * Return type of branch <code>success</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class SuccessReturnImpl implements SuccessReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(SuccessReturnImpl.class);

			private final Context ctx;


	        SuccessReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	private <T> T getConcreteUser(final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.BaseUser subject, final Class<T> userType) {
		for (final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser concreteUser: subject.getconcreteUser_ConcreteUser()) {
			if (userType.isAssignableFrom(concreteUser.getClass())) {
				return (T)concreteUser;
			}
		}
		throw new IllegalStateException("Did not find concrete user of type '" + userType.getSimpleName() + "'");
	}
	
	

	// sibs
	// container for retrieve user sib 'Retrieve Current User'.
	public AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult execute_POloMNMdEeWJV9QOcxWQYg(final Context ctx) {
		final Subject shiroSubj = SecurityUtils.getSubject();
	
		if (!shiroSubj.isAuthenticated()) {
			GUIEncounteredSignal sig = new GUIEncounteredSignal(ctx.callStack, "_POloMNMdEeWJV9QOcxWQYg");
			sig.setStatus(401);
			throw sig;
		}
	
		final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.BaseUserController subjectController = this.BaseUserController;
	
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.BaseUser subject = subjectController.read((Long)shiroSubj.getPrincipal());
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser concreteUser = getConcreteUser(subject, de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser.class);
		de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser value5 = concreteUser.getdywaSwitchedTo() != null ? concreteUser.getdywaSwitchedTo() : concreteUser;
		if (value5 != null) { // prevent null in lists
		Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getowners_ConcreteUser).get().add(value5);
		}
		// branch 'success'
		return execute_Jg3QYNMdEeWJV9QOcxWQYg(ctx);
	}
	// container for graph i/o 'success'.
	public AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult execute_Jg3QYNMdEeWJV9QOcxWQYg(final Context ctx) {
		return new AddTODOList_JgutgNMdEeWJV9QOcxWQYgResult(new SuccessReturnImpl(ctx));
	}
	
}  
