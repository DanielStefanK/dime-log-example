// Generator: info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)

package info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.addlist;

public class AddList_hGBmcN7nEeW7MO8ocVWj8QResult {
	
	private String branchName;
	
	public String getBranchName() {
		return this.branchName;
	}
	
	
	public AddList_hGBmcN7nEeW7MO8ocVWj8QResult(String branchName) {
		this.branchName = branchName;
	}
	
	
	public AddList_hGBmcN7nEeW7MO8ocVWj8QResult(String branchName, gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn) {
		this.branchName = branchName;
		this.gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn = gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	}
	
	private gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	
	public gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn getgui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn() {
		return gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn;
	}
	
	public static class gui__ArN44PvvEeWWodZOLky4iQAdd_ListReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList NewList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getNewList() {
			return NewList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewList")
		public void setNewList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList NewList) {
			this.NewList = NewList;
		}
	}
}
