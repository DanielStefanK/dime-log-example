package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public final class AddTODO_6e8l0NMbEeWJV9QOcxWQYg implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// context variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist;
		// direct dataflow variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry startNewEntry__KS6oNMbEeWJV9QOcxWQYg;

		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;

	@Inject
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYg(final BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	private Context createContext(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList list, de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry newEntry) {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		ctx.startNewEntry__KS6oNMbEeWJV9QOcxWQYg = newEntry;
		ctx.todolist = list;
		de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry value4 = newEntry;
		if (value4 != null) { // prevent null in lists
		Optional.ofNullable(ctx.todolist).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList::getentries_TODOEntry).get().add(value4);
		}
		
		return ctx;
	}

	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult execute(boolean isAuthenticationRequired,de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList list, de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry newEntry) {
		final Context ctx = createContext(list, newEntry);
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult executeInternal(final Context ctx) {
		return execute_jNQmYdgCEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private SuccessReturn success;
		
		public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult(SuccessReturn success) {
			this.branchName = "success";
			this.branchId = "_6fCscNMbEeWJV9QOcxWQYg";
			this.success = success;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public SuccessReturn getSuccessReturn() {
			return success;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>success</code>.
		 */
		public interface SuccessReturn {
		}
	
		/**
		 * Return type of branch <code>success</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class SuccessReturnImpl implements SuccessReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(SuccessReturnImpl.class);

			private final Context ctx;


	        SuccessReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	
	

	// sibs
	// container for graph i/o 'success'.
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult execute_6fCscNMbEeWJV9QOcxWQYg(final Context ctx) {
		return new AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult(new SuccessReturnImpl(ctx));
	}
	// container for atomic SIB 'LogInfoMessage'.
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult execute_gR8r0dgCEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.logInfoMessage("Todo item created");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'LogInfoMessage' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_mCyCAdgCEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
	}
	// container for atomic SIB 'PutToLoggingContext'.
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult execute_jNQmYdgCEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.putToContext("createdTodo", ctx.startNewEntry__KS6oNMbEeWJV9QOcxWQYg);
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'PutToLoggingContext' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_gR8r0dgCEeub4b__HYPHEN_MINUS__aIAJkSw(ctx);
	}
	// container for atomic SIB 'RemoveFromLoggingContext'.
	public AddTODO_6e8l0NMbEeWJV9QOcxWQYgResult execute_mCyCAdgCEeub4b__HYPHEN_MINUS__aIAJkSw(final Context ctx) {
		try {
			info.scce.dime.common.CommonNativeServiceLibrary.clearContext("createdTodo");
		}
		catch (Exception e) {
			e.printStackTrace();
			// branch 'failure'
			throw new IllegalStateException("SIB 'RemoveFromLoggingContext' has no successor defined for branch 'failure'");
		}
		// branch 'success'
		return execute_6fCscNMbEeWJV9QOcxWQYg(ctx);
	}
	
}  
