// Generator: info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)

package info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners;

public class ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult {
	
	private String branchName;
	
	public String getBranchName() {
		return this.branchName;
	}
	
	
	public ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult(String branchName) {
		this.branchName = branchName;
	}
	
	
	public ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult(String branchName, gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn) {
		this.branchName = branchName;
		this.gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn = gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	}
	
	private gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	
	public gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn getgui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn() {
		return gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn;
	}
	
	public static class gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser CurrentOwner;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentOwner")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser getCurrentOwner() {
			return CurrentOwner;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentOwner")
		public void setCurrentOwner(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser CurrentOwner) {
			this.CurrentOwner = CurrentOwner;
		}
	}
	
	public ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult(String branchName, gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn) {
		this.branchName = branchName;
		this.gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn = gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	private gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	
	public gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn() {
		return gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	public static class gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser;
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser getChosenUser() {
			return ChosenUser;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public void setChosenUser(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser) {
			this.ChosenUser = ChosenUser;
		}
	}
}
