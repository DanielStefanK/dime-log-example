// Generator: info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)

package info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageentries;

public class ManageEntries_bS1KsN7lEeW7MO8ocVWj8QResult {
	
	private String branchName;
	
	public String getBranchName() {
		return this.branchName;
	}
	
	
	public ManageEntries_bS1KsN7lEeW7MO8ocVWj8QResult(String branchName) {
		this.branchName = branchName;
	}
	
	
	public ManageEntries_bS1KsN7lEeW7MO8ocVWj8QResult(String branchName, gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn) {
		this.branchName = branchName;
		this.gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn = gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	}
	
	private gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	
	public gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn getgui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn() {
		return gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn;
	}
	
	public static class gui__vOKzMPvuEeWWodZOLky4iQAdd_TODOReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry NewEntry;
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewEntry")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry getNewEntry() {
			return NewEntry;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("NewEntry")
		public void setNewEntry(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry NewEntry) {
			this.NewEntry = NewEntry;
		}
	}
	
	public ManageEntries_bS1KsN7lEeW7MO8ocVWj8QResult(String branchName, gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn) {
		this.branchName = branchName;
		this.gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn = gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	}
	
	private gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	
	public gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn getgui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn() {
		return gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn;
	}
	
	public static class gui__9IttYPvsEeWWodZOLky4iQDelete_TODOReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry CurrentEntry;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentEntry")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry getCurrentEntry() {
			return CurrentEntry;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentEntry")
		public void setCurrentEntry(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOEntry CurrentEntry) {
			this.CurrentEntry = CurrentEntry;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
	}
}
