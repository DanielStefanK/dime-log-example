package info.scce.dime.logging;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for aggregating frontend logs
 *
 * @author danielstefank
 */

@Transactional
@Path("/_logging")
public class FrontendLoggingController {

  /** the logger for this class */

  private Logger LOGGER = LoggerFactory.getLogger(FrontendLoggingController.class);


  /**
   * Endpoint for posting frontend logs
   *
   * @param request     the request containing the logs and meta data
   * @return            the response
   */

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  public Response logFrontendErrors(FrontendErrorRequest request) {
    for (FrontendErrorRequest.Log l: request.getLogs()) {
      LOGGER.warn("[FRONTEND]: {}", l.toString());
    }
    return Response.ok().build();
  }
}
