// Generator: info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResult(GUI gui,Map<String,Map<String,TypeView>> branches)

package info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.addowner;

public class AddOwner_LIjH8NvFEeWsF7MALrkAZQResult {
	
	private String branchName;
	
	public String getBranchName() {
		return this.branchName;
	}
	
	
	public AddOwner_LIjH8NvFEeWsF7MALrkAZQResult(String branchName) {
		this.branchName = branchName;
	}
	
	
	public AddOwner_LIjH8NvFEeWsF7MALrkAZQResult(String branchName, gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn) {
		this.branchName = branchName;
		this.gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn = gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	private gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	
	public gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn getgui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn() {
		return gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn;
	}
	
	public static class gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn {
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList;
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList getCurrentList() {
			return CurrentList;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("CurrentList")
		public void setCurrentList(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList CurrentList) {
			this.CurrentList = CurrentList;
		}
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser;
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser getChosenUser() {
			return ChosenUser;
		}
		
		@com.fasterxml.jackson.annotation.JsonProperty("ChosenUser")
		public void setChosenUser(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser ChosenUser) {
			this.ChosenUser = ChosenUser;
		}
	}
}
