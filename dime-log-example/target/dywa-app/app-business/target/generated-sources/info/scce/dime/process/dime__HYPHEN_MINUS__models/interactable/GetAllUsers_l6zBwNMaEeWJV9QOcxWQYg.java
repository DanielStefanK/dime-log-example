package info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@info.scce.dime.process.RequiresDI
public final class GetAllUsers_l6zBwNMaEeWJV9QOcxWQYg implements DIMEProcess {

			// helper context for JSON serialization.
			@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
			public static class JSONContext extends info.scce.dime.process.JSONContext {

				// begin context variables
				// end context variables

				// begin direct dataflow variables
				// end direct dataflow variables

				// begin index variables of iterate sibs
				private int counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA = 0;
				// endindex variables of iterate sibs

				public static JSONContext toJSON(BeanManager bm, Context ctx) {
					final JSONContext result = new JSONContext();
					final info.scce.dime.rest.ObjectCache objectCache = new info.scce.dime.rest.ObjectCache();

					result.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA = ctx.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA;

					return result;
				}

				@Override
		        public Context toContext(BeanManager bm, ProcessCallFrame callStack) {
					final Context result = new Context();
		            result.beanManager = bm;
					result.callStack = callStack;



					result.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA = this.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA;

					return result;
		        }
			}

	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// context variables.
		private List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> realUsers = new ArrayList<>();
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser concreteuser;
		private List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> allUsers = new ArrayList<>();
		// index variables of iterate sibs.
		private int counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA = 0;

		public info.scce.dime.process.JSONContext toJSON() {
			return JSONContext.toJSON(beanManager, this);
        }
	}

	private final BeanManager beanManager;
	private final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController ConcreteUserController;

	@Inject
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYg(final BeanManager beanManager, de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController ConcreteUserController) {
		this.beanManager = beanManager;
		this.ConcreteUserController = ConcreteUserController;
	}

	private Context createContext() {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		
		return ctx;
	}

	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute(boolean isAuthenticationRequired) {
		final Context ctx = createContext();
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		final Context ctx = ((JSONContext) context).toContext(this.beanManager, callStack);

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult executeInternal(final Context ctx) {
		return execute_n_xKMNMaEeWJV9QOcxWQYg(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private SUccessReturn SUccess;
		
		public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult(SUccessReturn SUccess) {
			this.branchName = "SUccess";
			this.branchId = "_l7LcQNMaEeWJV9QOcxWQYg";
			this.SUccess = SUccess;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public SUccessReturn getSUccessReturn() {
			return SUccess;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>SUccess</code>.
		 */
		public interface SUccessReturn {
			public List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> getAllUsers();
		}
	
		/**
		 * Return type of branch <code>SUccess</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class SUccessReturnImpl implements SUccessReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(SUccessReturnImpl.class);

			private final Context ctx;


	        SUccessReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

			public List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> getAllUsers() {
				return new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(ctx.realUsers);
			}
		}
		

	
	

	// sibs
	// container for retrieve all of type (matching the given constraints) SIB 'Retrieve all ConcreteUser'.
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute_n_xKMNMaEeWJV9QOcxWQYg(final Context ctx) {
		final de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController domController = this.ConcreteUserController;
	
		// search for all objects of type matching the given contraints.
		final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser searchObject = domController.createSearchObject("");
		final List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> result = domController.findByProperties(searchObject);
		List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> value7 = result;
		if (value7 != null) { // prevent null in lists
		ctx.allUsers = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(value7);
		}
		if (result.isEmpty()) {
			// branch 'none found'
			return execute_l7LcQNMaEeWJV9QOcxWQYg(ctx);
		}
		else {
			// branch 'success'
			return execute_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA(ctx);
		}
	}
	// container for iterate complex SIB 'Iterate ConcreteUsers'.
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA(final Context ctx) {
		final int pos = ctx.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA++;
		List<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser> list = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(ctx.allUsers);
		if(list != null && pos < list.size()) {
			ctx.concreteuser = new ArrayList<de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser>(ctx.allUsers).get(pos);
			// branch 'next'
			return execute_Ew__HYPHEN_MINUS__d0PzLEea654knbZORoQ(ctx);
		}
		else {
			ctx.counterIterate_ConcreteUsers_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA = 0;
			// branch 'exit'
			return execute_l7LcQNMaEeWJV9QOcxWQYg(ctx);
		}
		
	}
	// container for SIB 'PutToContext' putting some values to context.
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute_eptbkNoyEeWXToNFm__HYPHEN_MINUS__DPVA(final Context ctx) {
		// put 'user'.
		de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser value8 = ctx.concreteuser;
		if (value8 != null) { // prevent null in lists
		ctx.realUsers.add(value8);
		}
	
		// branch 'success'
		return execute_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA(ctx);
	}
	// container for graph i/o 'SUccess'.
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute_l7LcQNMaEeWJV9QOcxWQYg(final Context ctx) {
		return new GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult(new SUccessReturnImpl(ctx));
	}
	// container for atomic SIB 'TextIsNull'.
	public GetAllUsers_l6zBwNMaEeWJV9QOcxWQYgResult execute_Ew__HYPHEN_MINUS__d0PzLEea654knbZORoQ(final Context ctx) {
		if (info.scce.dime.common.CommonNativeServiceLibrary.textIsNull(Optional.ofNullable(ctx.concreteuser).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser::getbaseUser).map(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.BaseUser::getpassword).orElse(new java.lang.String()))) {
			// branch 'true'
			return execute_ZdijkNowEeWXToNFm__HYPHEN_MINUS__DPVA(ctx);
		}
		else {
			// branch 'false'
			return execute_eptbkNoyEeWXToNFm__HYPHEN_MINUS__DPVA(ctx);
		}
	}
	
}  
