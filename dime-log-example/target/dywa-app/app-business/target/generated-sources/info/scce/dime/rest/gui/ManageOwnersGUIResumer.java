// generated by info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResumer
package info.scce.dime.rest.gui;

@javax.transaction.Transactional(dontRollbackOn = info.scce.dime.exception.GUIEncounteredSignal.class)
@javax.ws.rs.Path("/continue/_T77ycN7hEeW7MO8ocVWj8Q")
public class ManageOwnersGUIResumer extends info.scce.dime.process.GUIResumer {
	
	@javax.inject.Inject
	private info.scce.dime.gui.ProcessResumer processResumer;
	
	// controller for fetching dywa file entities
	@javax.inject.Inject
	private de.ls5.dywa.generated.util.DomainFileController domainFileController;
	
	@javax.inject.Inject
	private de.ls5.dywa.generated.rest.controller.ConcreteUserREST ConcreteUserREST;
	@javax.inject.Inject
	private de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.ConcreteUserController ConcreteUserController;
	@javax.inject.Inject
	private de.ls5.dywa.generated.rest.controller.TODOListREST TODOListREST;
	@javax.inject.Inject
	private de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo.TODOListController TODOListController;

	@javax.ws.rs.POST
	@javax.ws.rs.Path("Add_Owner/branch/public")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response continueAdd_OwnerBranch(info.scce.dime.rest.gui.model._UytoEPvuEeWWodZOLky4iQBranch output) {
		
		checkAuthentication(output.getCallStack(),"_T77ycN7hEeW7MO8ocVWj8Q/Add_Owner");
		
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
			 currentList;
			if (output.getCurrentList() != null) {
			// create new object
			if (output.getCurrentList().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
				if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
					final java.lang.String dywaName;
					if (output.getCurrentList().getDywaName() == null || output.getCurrentList().getDywaName().isEmpty()) {
						dywaName = "currentList";
					} else {
						dywaName = output.getCurrentList().getDywaName();
					}
					final long id;
					id = TODOListREST.create(dywaName);
					output.getCurrentList().setDywaId(id);
					//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
					TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList());
					currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
					) TODOListController.read(output.getCurrentList().getDywaId());
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }	}
			// transient object
			else if (output.getCurrentList().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
				if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
					final java.lang.String dywaName;
					if (output.getCurrentList().getDywaName() == null || output.getCurrentList().getDywaName().isEmpty()) {
						dywaName = "currentList";
					} else {
						dywaName = output.getCurrentList().getDywaName();
					}
					final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList transientObject;
					if (output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
						transientObject = TODOListController.createTransient(dywaName);
					}
					 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }
		
					//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
					TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList(), transientObject);
					currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
					) transientObject;
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }	}
			// regular object
				else {
					if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
						//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
						TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList());
						currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
						) TODOListController.read(output.getCurrentList().getDywaId());
					}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }		}
			
			}
			else {
				currentList = null;
			}
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
			 chosenUser;
			if (output.getChosenUser() != null) {
			// create new object
			if (output.getChosenUser().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
				if(output.getChosenUser() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
					final java.lang.String dywaName;
					if (output.getChosenUser().getDywaName() == null || output.getChosenUser().getDywaName().isEmpty()) {
						dywaName = "chosenUser";
					} else {
						dywaName = output.getChosenUser().getDywaName();
					}
					final long id;
					id = ConcreteUserREST.create(dywaName);
					output.getChosenUser().setDywaId(id);
					//update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ
					ConcreteUserREST.update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getChosenUser());
					chosenUser = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
					) ConcreteUserController.read(output.getChosenUser().getDywaId());
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getChosenUser().getClass()); }	}
			// transient object
			else if (output.getChosenUser().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
				if(output.getChosenUser() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
					final java.lang.String dywaName;
					if (output.getChosenUser().getDywaName() == null || output.getChosenUser().getDywaName().isEmpty()) {
						dywaName = "chosenUser";
					} else {
						dywaName = output.getChosenUser().getDywaName();
					}
					final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser transientObject;
					if (output.getChosenUser() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
						transientObject = ConcreteUserController.createTransient(dywaName);
					}
					 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getChosenUser().getClass()); }
		
					//update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ
					ConcreteUserREST.update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getChosenUser(), transientObject);
					chosenUser = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
					) transientObject;
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getChosenUser().getClass()); }	}
			// regular object
				else {
					if(output.getChosenUser() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
						//update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ
						ConcreteUserREST.update_AddOwnerConcreteUserSelectivex2_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getChosenUser());
						chosenUser = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
						) ConcreteUserController.read(output.getChosenUser().getDywaId());
					}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getChosenUser().getClass()); }		}
			
			}
			else {
				chosenUser = null;
			}
		final info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult.gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn guiReturn = new info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult.gui__UytoEPvuEeWWodZOLky4iQAdd_OwnerReturn();
		guiReturn.setCurrentList(currentList);
		guiReturn.setChosenUser(chosenUser);
		
		final info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult guiResult = 
		new info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult("Add_Owner", guiReturn);
		if(!"_T77ycN7hEeW7MO8ocVWj8Q".equals(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getPointer().split(":")[2])){
					output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).setPointer(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getMajorGUI());
		}
		if(!output.getCallStack().getCallFrames().isEmpty()) {
		
			if(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size() - 1).getPointer() == null) {
				return javax.ws.rs.core.Response.status(422).build();
			}
		}
		final Object result = this.processResumer.resumeFromGUI(output.getCallStack(), guiResult);
		return javax.ws.rs.core.Response.ok(result).build();
	}
	@javax.ws.rs.POST
	@javax.ws.rs.Path("Remove_Owner/branch/public")
	@javax.ws.rs.Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	@javax.ws.rs.Consumes(javax.ws.rs.core.MediaType.APPLICATION_JSON)
	public javax.ws.rs.core.Response continueRemove_OwnerBranch(info.scce.dime.rest.gui.model.__11lgPvtEeWWodZOLky4iQBranch output) {
		
		checkAuthentication(output.getCallStack(),"_T77ycN7hEeW7MO8ocVWj8Q/Remove_Owner");
		
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
			 currentList;
			if (output.getCurrentList() != null) {
			// create new object
			if (output.getCurrentList().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
				if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
					final java.lang.String dywaName;
					if (output.getCurrentList().getDywaName() == null || output.getCurrentList().getDywaName().isEmpty()) {
						dywaName = "currentList";
					} else {
						dywaName = output.getCurrentList().getDywaName();
					}
					final long id;
					id = TODOListREST.create(dywaName);
					output.getCurrentList().setDywaId(id);
					//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
					TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList());
					currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
					) TODOListController.read(output.getCurrentList().getDywaId());
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }	}
			// transient object
			else if (output.getCurrentList().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
				if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
					final java.lang.String dywaName;
					if (output.getCurrentList().getDywaName() == null || output.getCurrentList().getDywaName().isEmpty()) {
						dywaName = "currentList";
					} else {
						dywaName = output.getCurrentList().getDywaName();
					}
					final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList transientObject;
					if (output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
						transientObject = TODOListController.createTransient(dywaName);
					}
					 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }
		
					//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
					TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList(), transientObject);
					currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
					) transientObject;
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }	}
			// regular object
				else {
					if(output.getCurrentList() instanceof de.ls5.dywa.generated.rest.types.TODOList) {
						//update_ManageOwnersTODOListSelectivex2_T77ycN7hEeW7MO8ocVWj8Q
						TODOListREST.update_AddOwnerTODOListSelectivex3_LIjH8NvFEeWsF7MALrkAZQ((de.ls5.dywa.generated.rest.types.TODOList)output.getCurrentList());
						currentList = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList
						) TODOListController.read(output.getCurrentList().getDywaId());
					}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentList().getClass()); }		}
			
			}
			else {
				currentList = null;
			}
			final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
			 currentOwner;
			if (output.getCurrentOwner() != null) {
			// create new object
			if (output.getCurrentOwner().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_CREATE_NEW) {
				if(output.getCurrentOwner() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
					final java.lang.String dywaName;
					if (output.getCurrentOwner().getDywaName() == null || output.getCurrentOwner().getDywaName().isEmpty()) {
						dywaName = "currentOwner";
					} else {
						dywaName = output.getCurrentOwner().getDywaName();
					}
					final long id;
					id = ConcreteUserREST.create(dywaName);
					output.getCurrentOwner().setDywaId(id);
					//update_ManageOwnersConcreteUserSelectivex1_T77ycN7hEeW7MO8ocVWj8Q
					ConcreteUserREST.update_HomeConcreteUserSelectivex1x2x2x2x2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getCurrentOwner());
					currentOwner = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
					) ConcreteUserController.read(output.getCurrentOwner().getDywaId());
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentOwner().getClass()); }	}
			// transient object
			else if (output.getCurrentOwner().getDywaId() == info.scce.dime.util.Constants.DYWA_ID_TRANSIENT) {
				if(output.getCurrentOwner() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
					final java.lang.String dywaName;
					if (output.getCurrentOwner().getDywaName() == null || output.getCurrentOwner().getDywaName().isEmpty()) {
						dywaName = "currentOwner";
					} else {
						dywaName = output.getCurrentOwner().getDywaName();
					}
					final de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser transientObject;
					if (output.getCurrentOwner() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
						transientObject = ConcreteUserController.createTransient(dywaName);
					}
					 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentOwner().getClass()); }
		
					//update_ManageOwnersConcreteUserSelectivex1_T77ycN7hEeW7MO8ocVWj8Q
					ConcreteUserREST.update_HomeConcreteUserSelectivex1x2x2x2x2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getCurrentOwner(), transientObject);
					currentOwner = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
					) transientObject;
				}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentOwner().getClass()); }	}
			// regular object
				else {
					if(output.getCurrentOwner() instanceof de.ls5.dywa.generated.rest.types.ConcreteUser) {
						//update_ManageOwnersConcreteUserSelectivex1_T77ycN7hEeW7MO8ocVWj8Q
						ConcreteUserREST.update_HomeConcreteUserSelectivex1x2x2x2x2_gF__HYPHEN_MINUS__WEMwMEeWZVvvwxpYu8A((de.ls5.dywa.generated.rest.types.ConcreteUser)output.getCurrentOwner());
						currentOwner = (de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.ConcreteUser
						) ConcreteUserController.read(output.getCurrentOwner().getDywaId());
					}
		 else { throw new java.lang.IllegalArgumentException("Unexpected type " + output.getCurrentOwner().getClass()); }		}
			
			}
			else {
				currentOwner = null;
			}
		final info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult.gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn guiReturn = new info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult.gui___11lgPvtEeWWodZOLky4iQRemove_OwnerReturn();
		guiReturn.setCurrentList(currentList);
		guiReturn.setCurrentOwner(currentOwner);
		
		final info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult guiResult = 
		new info.scce.dime.gui.dime__HYPHEN_MINUS__models.gui.home.manageowners.ManageOwners_T77ycN7hEeW7MO8ocVWj8QResult("Remove_Owner", guiReturn);
		if(!"_T77ycN7hEeW7MO8ocVWj8Q".equals(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getPointer().split(":")[2])){
					output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).setPointer(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size()-1).getMajorGUI());
		}
		if(!output.getCallStack().getCallFrames().isEmpty()) {
		
			if(output.getCallStack().getCallFrames().get(output.getCallStack().getCallFrames().size() - 1).getPointer() == null) {
				return javax.ws.rs.core.Response.status(422).build();
			}
		}
		final Object result = this.processResumer.resumeFromGUI(output.getCallStack(), guiResult);
		return javax.ws.rs.core.Response.ok(result).build();
	}
	
}
