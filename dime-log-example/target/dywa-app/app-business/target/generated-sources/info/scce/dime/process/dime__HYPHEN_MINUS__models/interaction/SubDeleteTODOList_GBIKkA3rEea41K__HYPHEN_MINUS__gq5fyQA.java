package info.scce.dime.process.dime__HYPHEN_MINUS__models.interaction;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import java.util.Date;
import java.util.UUID;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import info.scce.dime.exception.GUIEncounteredSignal;
import info.scce.dime.exception.GUIEncounteredSignal.GUIInfo;
import info.scce.dime.process.CallFrame;
import info.scce.dime.process.DIMEProcess;
import info.scce.dime.process.DIMEProcessContext;
import info.scce.dime.process.JSONContext;
import info.scce.dime.process.ProcessCallFrame;
import info.scce.dime.util.CDIUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

public final class SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA implements DIMEProcess {


	// attributes shaping the context variables.
	public static class Context implements DIMEProcessContext {

		// bean manager
		private BeanManager beanManager;
		
		// current eventID
		private String eventId;
		
		// last MajorSIBId
		private String lastMajorSIBId;
		
		// current MajorGUI
		private GUIInfo majorGUIState = null;

		// stack variables
		private ProcessCallFrame callStack = new ProcessCallFrame();

		// direct dataflow variables.
		private de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList startTodolist1_KsBzcA3rEea41K__HYPHEN_MINUS__gq5fyQA;

		public info.scce.dime.process.JSONContext toJSON() {
			// this process has no context variables that need to be preserved
			return null;
        }
	}

	private final BeanManager beanManager;

	@Inject
	public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQA(final BeanManager beanManager) {
		this.beanManager = beanManager;
	}

	private Context createContext(de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist1) {
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;

		// store inputs
		ctx.startTodolist1_KsBzcA3rEea41K__HYPHEN_MINUS__gq5fyQA = todolist1;
		
		return ctx;
	}

	public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult execute(boolean isAuthenticationRequired,de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList todolist1) {
		final Context ctx = createContext(todolist1);
		ctx.callStack.setAuthenticationRequired(isAuthenticationRequired);

		return executeInternal(ctx);
	}
	
	

	@Override
	public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult continueExecution(ProcessCallFrame callStack, info.scce.dime.process.JSONContext context, String sibId, Object slgResult) {
		assert context == null;
		final Context ctx = new Context();
		ctx.beanManager = this.beanManager;
		ctx.callStack = callStack;

		switch (sibId) {
			default: throw new IllegalStateException("Unknown continuation point '" + sibId + '\'');
		}
	}

	private SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult executeInternal(final Context ctx) {
		return execute_HUGOUA3rEea41K__HYPHEN_MINUS__gq5fyQA(ctx);
	}
	
	/**
	 * The return type for this process. It stores the corresponding branch name 
	 * as well as the corresponding result for the branch.
	 */
	public static class SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult implements info.scce.dime.process.DIMEProcessResult<Void> {
		private String branchName;
		private String branchId;
		private DeletedReturn deleted;
		
		public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult(DeletedReturn deleted) {
			this.branchName = "deleted";
			this.branchId = "_GBjBUA3rEea41K__HYPHEN_MINUS__gq5fyQA";
			this.deleted = deleted;
		}
		
		public String getBranchName() {
			return branchName;
		}
		
		public String getBranchId() {
			return branchId;
		}
		
		public DeletedReturn getDeletedReturn() {
			return deleted;
		}

	}
	
	// model branches.
		/**
		 * Interface definition for return type of branch <code>deleted</code>.
		 */
		public interface DeletedReturn {
		}
	
		/**
		 * Return type of branch <code>deleted</code> accessing the 
		 * corresponding values in the process context, instead of storing
		 * the values locally.
		 */
		static class DeletedReturnImpl implements DeletedReturn {
			
			
			 /** the logger for this class */
					
			private Logger LOGGER = LoggerFactory.getLogger(DeletedReturnImpl.class);

			private final Context ctx;


	        DeletedReturnImpl(Context ctx) {
	            this.ctx = ctx;
	        }

		}
		

	
	

	// sibs
	// container for graph i/o 'deleted'.
	public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult execute_GBjBUA3rEea41K__HYPHEN_MINUS__gq5fyQA(final Context ctx) {
		return new SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult(new DeletedReturnImpl(ctx));
	}
	// container for graph abstraction 'DeleteTODOList' and sub process 'DeleteTODOList'.
	public SubDeleteTODOList_GBIKkA3rEea41K__HYPHEN_MINUS__gq5fyQAResult execute_HUGOUA3rEea41K__HYPHEN_MINUS__gq5fyQA(final Context ctx) {
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQ instance = CDIUtil.getManagedInstance(ctx.beanManager, info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQ.class);
	
		final info.scce.dime.process.dime__HYPHEN_MINUS__models.interactable.DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQ.DeleteTODOList_xtyqIN8MEeWlD7knc3NOdQResult result = instance.execute(false, ctx.startTodolist1_KsBzcA3rEea41K__HYPHEN_MINUS__gq5fyQA);
	
		switch(result.getBranchName()) {
		case "deleted": {
			// branch 'deleted'
			return execute_GBjBUA3rEea41K__HYPHEN_MINUS__gq5fyQA(ctx);
		}
			default: throw new IllegalStateException("SIB 'DeleteTODOList' has no successor defined for branch '" + result.getBranchName() + '\'');
		}
	}
	
	
}  
