
		// generated by info.scce.dime.generator.rest.GUIProgressGenerator#generateGUIResumeInput
		
		package info.scce.dime.rest.gui.model;
		
		public class _UytoEPvuEeWWodZOLky4iQBranch{
			

			info.scce.dime.process.ProcessCallFrame callStack;
			
			@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
			@com.codingrodent.jackson.crypto.Encrypt
			@com.fasterxml.jackson.databind.annotation.JsonSerialize(using = info.scce.dime.rest.ContextIndependentSerializer.class)
			@com.fasterxml.jackson.databind.annotation.JsonDeserialize(using = info.scce.dime.rest.ContextIndependentDeserializer.class)
			public info.scce.dime.process.ProcessCallFrame getCallStack() {
				return callStack;
			}
			

			@com.fasterxml.jackson.annotation.JsonProperty("dywaData")
			public void setCallStack(info.scce.dime.process.ProcessCallFrame callStack) {
				this.callStack = callStack;
			}
			private de.ls5.dywa.generated.rest.types.TODOList
			 currentList;
			
			@com.fasterxml.jackson.annotation.JsonProperty("currentList")
			public de.ls5.dywa.generated.rest.types.TODOList
			 getCurrentList() {
				return currentList;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("currentList")
			public void setCurrentList(de.ls5.dywa.generated.rest.types.TODOList
			 currentList) {
				this.currentList = currentList;
			}
			private de.ls5.dywa.generated.rest.types.ConcreteUser
			 chosenUser;
			
			@com.fasterxml.jackson.annotation.JsonProperty("chosenUser")
			public de.ls5.dywa.generated.rest.types.ConcreteUser
			 getChosenUser() {
				return chosenUser;
			}
			
			@com.fasterxml.jackson.annotation.JsonProperty("chosenUser")
			public void setChosenUser(de.ls5.dywa.generated.rest.types.ConcreteUser
			 chosenUser) {
				this.chosenUser = chosenUser;
			}
		}
