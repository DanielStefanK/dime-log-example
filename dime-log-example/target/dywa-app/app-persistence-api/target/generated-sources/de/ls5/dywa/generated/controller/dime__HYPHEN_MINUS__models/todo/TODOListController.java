/* generated by info.scce.dime.generator.scheme.ControllerGenerator */
package de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.todo;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.todo.TODOList;

public interface TODOListController {

	TODOList read(java.lang.Long id);

	java.util.List<TODOList> findByProperties(TODOList searchObject);

	TODOList findFirstByProperties(TODOList searchObject);

	java.util.Set<TODOList> fetch();

	java.util.Set<TODOList> fetchByName(java.lang.String name);

	TODOList create(java.lang.String name);
	TODOList createTransient(java.lang.String name);

	TODOList createSearchObject(java.lang.String name);

	java.util.Set<TODOList> fetchWithSubtypes();

	void delete(TODOList entity);

	void deleteWithIncomingReferences(TODOList entity);
}
